# PhD-Thesis

[![Pipeline status](https://gitlab.com/kevinkuns/phd-thesis/badges/master/pipeline.svg)](https://gitlab.com/kevinkuns/phd-thesis/commits/master)

A PDF of the thesis and defense slides are automatically generated after each commit.
*  [Thesis](https://gitlab.com/kevinkuns/phd-thesis/-/jobs/artifacts/master/file/main.pdf?job=pdf)
*  [Slides](https://gitlab.com/kevinkuns/phd-thesis/-/jobs/artifacts/master/file/Slides/slides.pdf?job=pdf)
