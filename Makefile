.PHONY: all
all: main

main: main
# need nonstopmode so that ci doesn't hang if there's an error
	latexmk -f -interaction=nonstopmode -pdf $@

.PHONY: clean
clean:
	latexmk -c
#	rm -f *_flymake*
	find . -name "*flymake*" -type f -delete
	find . -name "*aux" -type f -delete
	find . -name "*.log" -type f -delete
