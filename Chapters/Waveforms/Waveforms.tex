\chapter{Compact Binary Waveforms for Combined Networks of Ground and Space Detectors}
\label{chap:waveforms}

Analyzing the gravitational wave signal from a single source with a network of both ground and space detectors is challenging for a few reasons. First, since the space detectors are sensitive to lower frequencies than the ground detectors, the signals will stay in the space detectors' sensitivity band longer than the ground detectors' band; see Fig.~\ref{fig:waveform-amplitude} below. While the signal from a ground detector can be computed by projecting the gravitational wave signal onto the antenna pattern for that detector at the time the wave hits the detector, space detectors have to account for the Doppler shift of the wave's frequency as well as the changing antenna pattern caused by the motion of the detector in its orbit. Sec.~\ref{sec:duration} gives rough estimates for the timescales over which these time dependent effects are important.

Second, since the waves will often merge in the ground detectors' band, the merger and ringdown must be computed in addition to the inspiral. Waveforms used for low frequency missions such as LISA or TianQin, see for example \cite{Cutler1998, Berti2005, Barack2004, Feng2019}, include only the inspiral, and the current methods of calculating high frequency waveforms that include the merger and ringdown do not include the time dependent effects necessary for the low frequencies.

Finally, on a more practical level, since the low frequency waveforms last for so long, it is impractical to compute them with linearly spaced frequency points as is currently done for these high frequency waveforms. Any attempt to add the time dependence to the high frequency waveforms will be further complicated by the need to generate the waveforms at logarithmically spaced frequency points. One approach is generate the waveforms in two pieces: a high frequency linearly spaced waveform, and a low frequency logarthmically spaced waveform with Doppler shifts and time dependent antenna patterns built in. These two waveforms could then be joined, though this is difficult to achieve in practice.

For a detector such as TianGO, which is sensitive to an intermediate frequency band where a source will both spend a significant amount of time and merge, it is especially important to account for both the merger and ringdown as well as the time dependence and Doppler shifts.

This appendix describes such a waveform that can be simultaneously used for both ground and space detectors. Sec.~\ref{sec:basic-waveform} describes the basic waveform from Refs.~\cite{Ajith2008, Ajith2007}. Sec.~\ref{sec:ground-projection} then describes the projection of this basic waveform onto ground detectors, and Sec.~\ref{sec:space-projection} describes how the waveform used to analyze low frequency LISA signals from Ref.~\cite{Cutler1998} can be modified to project the waveform from Sec.~\ref{sec:basic-waveform} onto a space detector. Since the waveform is analytic, it is also straightforward to evaluate at logarithmically spaced frequency points.

Throughout this appendix the following mass quantities are used. The mass of each binary in the compact binary system is $M_1$ and $M_2$, and the total mass of the system is $M=M_1 + M_2$. The reduced mass is $\mu = M_1M_2/M$ and the symmetric mass ratio is $\eta = M_1M_2/M^2$. The chirp mass is
\begin{equation}
\mathcal{M} = \mu^{3/5}M^{2/5} = \eta^{3/5}M.
\end{equation}

\section{Time Dependent Effects of Low Frequency Waveforms}
\label{sec:duration}

The time it takes a binary system to coalesce from the time it is at the frequency $f$ is~\cite{Maggiore2008}
\begin{equation}
\tau = \frac{1}{\pi^{8/3}} \frac{5}{256} \left(\frac{GM_\odot}{c^3}\right)^{-5/3}
\left(\frac{\mathcal{M}}{M_\odot}\right)^{-5/3} f^{-8/3}
\end{equation}
Less massive systems therefore merge slower than more massive systems. If the frequency at which a gravitational wave enters a detector's band is sufficiently low, the time until merger will be sufficiently long that Doppler shifts and time dependent antenna patterns will need to be accounted for. In this section we give estimates for when these effects become important.

\paragraph{Doppler shift} This estimate for the magnitude of the Doppler shifts follows Ref.~\cite{Maggiore2008}. For a gravitational wave of frequency $f_\t{gw}$, the observed frequency in a detector moving with velocity $\v{v}$ with respect to the source is
\begin{equation}
f = f_\t{gw} \lp 1 + \frac{\v{v}\cdot\rf{r}}{c}\rp,
\end{equation}
where $\rf{r}$ is the unit vector in the direction of the source. Therefore, the change in frequency over a time $T$ is $\Delta f = f_\t{gw} \Delta v/c$ where $\Delta v$ is the change in velocity in the direction of the source over this time. Let the rotational velocity of the detector be  $v_\t{rot} = R\omega_\t{rot}$ where $R$ is the radius of the orbit. If we are considering the rotation of the detector around the sun then $R=R_\odot$, and if we are consider the rotation of the earth around its axis $R = R_\oplus$. These estimates are in the worst case where the detector is at the equator and facing the source head on. The change in angle of the detector around the axis of rotation is $\Delta\theta = T\omega_\t{rot}$. If $\Delta\theta \ll 1$
\begin{equation}
\Delta v \sim v_\t{rot}\Delta\theta \sim Tv_{\t{rot}}\omega_{\t{rot}}
\sim R\omega_\t{rot}^2 T.
\end{equation}

If the source is observed for a time $T$, then the frequency resolution is $\Delta f = 1/T$. The Doppler shift is not important if the frequency shift due to the Doppler effect is less than this frequency resolution:
\begin{equation}
f_\t{gw} R \frac{\omega_\t{rot}^2}{c} T \lesssim \frac{1}{T}
\end{equation}
or
\begin{equation}
T\lesssim \frac{1}{\omega_\t{rot}} \sqrt{\frac{c}{f_\t{gw} R}}.
\label{doppler-T-requirement}
\end{equation}
For the rotation of the Earth about its axis, this is
\begin{equation}
T_\oplus \lesssim 50\,\t{min}\; \sqrt{\frac{1\,\t{kHz}}{f_\t{gw}}},
\end{equation}
and for the rotation a detector around the sun, this is
\begin{equation}
T_\odot \lesssim 120\,\t{min}\; \sqrt{\frac{1\,\t{kHz}}{f_\t{gw}}}.
\end{equation}

There is a distance between two binaries beyond which no stable orbit exists. This orbit is called the innermost stable circular orbit (ISCO) and occurs at a frequency of
\begin{equation}
f_\t{ISCO} = \frac{1}{12\sqrt{6}\pi} \left(\frac{c^3}{G M_\odot}\right)
\left(\frac{M_\odot}{M_\t{tot}}\right).
\end{equation}
This is roughly the frequency at which the merger occurs and, since the gravitational wave frequency is twice the orbital frequency, the maximum frequency observed in a detector that sees the merger and ringdown is a little more than $f_\t{gw} = 2f_\t{ISCO}$. This is the value that should be used in the estimates in \eqref{doppler-T-requirement}.

\begin{table}
\centering
\begin{tabular}{|r|r|r|r|r|r|r|r|}
\hline
$f$ & $M_\t{tot}/M_\odot$ & $\tau$ & $2f_\t{ISCO}$ & $T_\odot$ & $T_\oplus$ & $\abs{\delta F/F}_\odot$ & $\abs{\delta F/F}_\oplus$ \\

\hline

$10\,\t{Hz}$ & $8$ & $170\,\t{s}$ & $550\,\t{Hz}$ & $160\,\t{min}$ & $67\,\t{min}$ & $7.3\times 10^{-5}$ & $2.6\times 10^{-2}$ \\

$10\,\t{Hz}$ & $80$ & $3.8\,\t{s}$ & $55\,\t{Hz}$ & $8.5\,\t{hr}$ & $3.6\,\t{hr}$ & $1.5\times 10^{-6}$ & $5.6\times 10^{-4}$\\

$10\,\t{Hz}$ & $800$ & $81\,\t{ms}$ & $5.5\,\t{Hz}$ & $27\,\t{hr}$ & $11.2\,\t{hr}$ & $3.2\times 10^{-8}$ & $1.2\times 10^{-5}$\\

\hline

$10\,\t{mHz}$ & $8$ & $550\,\t{yr}$ & $550\,\t{Hz}$ & $160\,\t{min}$ & $67\,\t{min}$ & 1 & 1 \\

$10\,\t{mHz}$ & $80$ & $11.9\,\t{yr}$ & $55\,\t{Hz}$ & $8.5\,\t{hr}$ & $3.6\,\t{hr}$ & 1 & 1\\

$10\,\t{mHz}$ & $800$ & $95\,\t{days}$ & $5.5\,\t{Hz}$ & $27\,\t{hr}$ & $11.2\,\t{hr}$ & 1 & 1\\

\hline
\end{tabular}
\caption[Importance of Doppler shifts and time dependent antenna patterns]{Importance of Doppler shifts and time dependent antenna patterns. The time to coalescence $\tau$ is for the frequency $f$ in the table, the maximum allowable times in band $T_{\odot,\oplus}$ are for $2f_\t{ISCO}$, and the magnitude of the relative antenna pattern change $\abs{\delta F/F}_{\odot,\oplus}$ are for the duration $\tau$. As described in the text, for Doppler shifts to be unimportant $\tau < T_\odot, T_\oplus$, and for antenna pattern variations to be unimportant $\abs{\delta F/F}_{\odot,\oplus} \ll 1$. The time dependent effects are thus negligible for ground detectors with sources entering their sensitivity band around $10\,\t{Hz}$, but are significant for space detectors with sources entering their sensitivity bands extending down to $10\,\t{mHz}$ or lower.}
\label{tab:duration}
\end{table}


\paragraph{Amplitude modulation} The antenna patterns for the plus and cross polarizations are
\begin{equation}
F_+ = \frac{1}{2}\lp 1 + \cos^2\theta\rp \cos 2\phi, \qquad
F_\times = \cos\theta \sin 2\phi
\end{equation}
For the rest of this estimate we take the maximum amplitude to get the worst case amplitude modulation. The maximum values of the rate of change of these amplitudes is
\begin{equation}
\abs{\dot{F}_+} = \abs{\dot{F}_\times} = 2\dot{\phi} = 2\omega_\t{rot}
\end{equation}
and the change in the amplitudes during a time $T$ is $\delta F_{+, \times} = \dot{F}_{+, \times} T$. The maximum value of the relative intensities is thus
\begin{equation}
\abs{\frac{\delta F_+}{F_+}}_\t{max} = \abs{\frac{\delta F_\times}{F_\times}}_\t{max} \sim 2\omega_\t{rot} T.
\end{equation}
For the rotation of the Earth about its axis this is
\begin{equation}
\abs{\frac{\delta F}{F}}_{\t{max}, \oplus} \sim  1.5\times 10^{-4}\; \frac{T}{1\,\t{s}},
\end{equation}
and for the rotation of the detector about the sun this is
\begin{equation}
\abs{\frac{\delta F}{F}}_{\t{max}, \odot} \sim 4\times 10^{-7}\; \frac{T}{1\,\t{s}} \sim 2.4\times 10^{-5}\; \frac{T}{1\,\t{min}}.
\end{equation}
Tab.~\ref{tab:duration} shows the estimates of the importance of Doppler shifts and time dependent antenna patterns for various sources as detected in a ground or space detector. It is assumed that the detector sees the source for the entire frequency band from $f$ to $2f_\t{ISCO}$. As the table shows, the effects described in this section are significant for space detectors but not for ground detectors. These effects may start to become important for the third generation of ground based gravitational wave detectors which will be sensitive to lower frequencies, however.

\section{Basic Waveform}
\label{sec:basic-waveform}

The waveform described in Refs.~\cite{Ajith2008, Ajith2007} is an analytic phenomenological waveform found by matching a numerical relativity waveform with a post Newtonian wave form. It is written in the frequency domain as
\begin{equation}
u(f) = A_\t{eff}(f)\,\e^{\i\Psi_\t{eff}(f)}
\label{basic-wf}
\end{equation}
where the amplitude is broken up into an inspiral, merger, and ringdown phase:
\begin{equation}
A_\t{eff}(f) = C\begin{cases}
(f/f_\t{merg})^{-7/6} & f < f_\t{merg}\\
(f/f_\t{merg})^{-2/3} & f_\t{merg} < f < f_\t{ring}\\
w\mathcal{L}(f, f_\t{ring}, \sigma) & f_\t{ring} < f < f_\t{cut}
\end{cases}.
\end{equation}
The ringdown is described by a Lorentzian of width $\sigma$ centered on the ringdown frequency $f_\t{ring}$
\begin{equation}
\mathcal{L}(f, f_\t{ring}, \sigma) = \frac{1}{2\pi}
\frac{\sigma}{(f - f_\t{ring})^2 + \sigma^2/4},
\end{equation}
and
\begin{equation}
w = \frac{\pi\sigma}{2} \left(\frac{f_\t{ring}}{f_\t{merg}}\right)
\end{equation}
is chosen to make the waveform continuous at the transition between the merger and ringdown. The overall amplitude is
\begin{equation}
C = \sqrt{\frac{5}{24}} \frac{c^{1/6}}{\pi^{2/3}D_L}
\left( \frac{G M_\odot}{c^2}\right)^{5/6}
\left(\frac{\mathcal{M}}{M_\odot}\right)^{5/6} f_\t{merg}^{-7/6}.
\end{equation}
where $\mathcal{M}$ is the chirp mass and $D_L$ is the luminosity distance.

The merger, ringdown, and cutoff frequencies as well as the Lorentzian width are given by matching to a numerical relativity waveform. If $\alpha_1= f_\t{merg}$, $\alpha_2=f_\t{ring}$, $\alpha_3=f_\t{cut}$, and $\alpha_4 = \sigma$, the parameters are
\begin{equation}
\alpha_k = \frac{c^3}{\pi GM_\odot}\left(\frac{M_\odot}{M}\right)
\left(a_k\eta^2 + b_k\eta + c_k\right),
\end{equation}
where the constants $a_k, b_k,$ and $c_k$ are given in Tab.~\ref{tab:alpha-fits}

\begin{table}
\centering
\begin{tabular}{|r|r|r|r|}
\hline
Quantity & $a_k$ & $b_k$ & $c_k$\\
\hline
$f_\t{ring}$ & $5.9411\times 10^{-1}$ & $8.9794\times 10^{-2}$ & $1.9111\times 10^{-1}$\\
$f_\t{merg}$ & $2.9740\times 10^{-1}$ & $4.4810\times 10^{-2}$ & $9.5560\times 10^{-2}$\\
$f_\t{cut}$ & $8.4845\times 10^{-1}$ & $1.2848\times 10^{-1}$ & $2.7299\times 10^{-1}$\\
$\sigma$ & $5.0801\times 10^{-1}$ & $7.7515\times 10^{-2}$ &  $2.2369\times 10^{-2}$ \\
\hline
\end{tabular}
\caption{Parameters for waveform frequencies and Lorentzian width.}
\label{tab:alpha-fits}
\end{table}

The phase is
\begin{equation}
\Psi_\t{eff}(f) = 2\pi ft_0 + \psi_0
+ \sum_{k=0}^7 \psi_k f^{(k-5)/3}
\end{equation}
where
\begin{equation}
\psi_k = \frac{1}{\eta} \left(\frac{c^3}{\pi GM_\odot}\right)^{(5-k)/3}
\left(\frac{M}{M_\odot}\right)^{(k-5)/3} (x_k\eta^2 + y_k\eta + z_k).
\end{equation}
The phase parameters are given in Tab.~\ref{tab:psi-fits}.

\begin{table}
\centering
\begin{tabular}{|r|r|r|r|}
\hline
$k$ & $x_k$ & $y_k$ & $z_k$\\
\hline
$0$ & $1.7516\times 10^{-1}$ & $7.9483\times 10^{-2}$ & $-7.2390\times 10^{-2}$\\
$1$ & 0 & 0 & 0\\
$2$ & $-5.1571\times 10^{1}$ & $-1.7595\times 10^{1}$ & $1.3253\times 10^{1}$\\
$3$ & $6.5866\times 10^{2}$ & $1.7803\times 10^{2}$ & $-1.5972\times 10^{2}$\\
$4$ & $-3.9031\times 10^{3}$ & $-7.7493\times 10^{2}$ & $8.8195\times 10^{2}$\\
$5$ & 0 & 0 & 0\\
$6$ & $-2.4874\times 10^{4}$ & $-1.4892\times 10^{3}$ & $4.4588\times 10^{3}$\\
$7$ & $2.5196\times 10^{4}$ & $3.3970\times 10^{2}$ & $-3.9573\times 10^{3}$\\
\hline
\end{tabular}
\caption{Parameters for waveform phase.}
\label{tab:psi-fits}
\end{table}

\section{Projection onto Ground Detectors}
\label{sec:ground-projection}

Projecting the gravitational wave signal \eqref{basic-wf} onto a ground detector is relatively straightforward since the detector itself can be treated as fixed in time. We use \eqref{basic-wf} to write the plus and cross polarizations as~\cite{Maggiore2008}
\begin{subequations}
\begin{align}
h_+(f) &= A_\t{eff}(f)\,\e^{\i\Psi_\t{eff}(f)}\, \left(\frac{1 + \cos^2\iota}{2}\right) \\
h_\times(f) &= A_\t{eff}(f)\,\e^{\i[\Psi_\t{eff}(f) + \pi/2]}\,\cos\iota,
\end{align}
\end{subequations}
where $\iota$ is the source inclination. The antenna patterns for an interferometer are
\begin{subequations}
\label{antenna-patterns}
\begin{align}
F_+ &= \left(\frac{1 + \cos^2\theta}{2}\right)\cos 2\phi \cos 2\psi
- \cos\theta \sin 2\phi \sin 2\psi \\
F_\times &= \left(\frac{1 + \cos^2\theta}{2}\right)\cos 2\phi \sin 2\psi
+ \cos\theta \sin 2\phi \cos 2\psi,
\end{align}
\end{subequations}
where $\phi$ and $\theta$ are the azimuthal and polar angles, respectively, of the source relative to the detector and $\psi$ is the polarization phase. Instead of $\phi$ and $\theta$, right ascension $\alpha = \phi$ and declination $\delta = \pi/2 - \theta$ are often used. The waveform observed in a given detector is then
\begin{equation}
h(f) = F_+ h_+(f) + F_\times h_\times(f).
\end{equation}

\section{Projection onto Space Detectors}
\label{sec:space-projection}

We follow the approach of Ref.~\cite{Cutler1998} to modify the waveform \eqref{basic-wf} to account for the Doppler shifts and the time dependence of the space detector antenna pattern. The strategy to account for the time dependence is to find the source location in the frame of the detector as a function of time. The source location relative to the ecliptic plane $\phi$, $\theta$, and $\psi$ is fixed and the location in the detector frame $\tilde{\phi}(t), \tilde{\theta}(t)$, and $\tilde{\psi}(t)$ is a function of time. Once these angles are known, they are used to calculate the Doppler shift and the antenna patterns \eqref{antenna-patterns} as a function of time. Finally, a post-Newtonian expansion is used to find the frequency of the waveform as a function of time so that the waveform can be written in the frequency domain.

The post-Newtonian expansion is done in the parameter~\cite{Cutler1998, Cutler1994}
\begin{equation}
x = \left(\frac{G}{c^3}\pi M_\odot\right)^{2/3} \frac{\mathcal{M}}{\mu}
\left[\left(\frac{\mathcal{M}}{M_\odot}\right) f\right]^{2/3}.
\end{equation}
The time as a function of frequency is
\begin{equation}
t_f = t_c
- t_x \left[1 + \frac{4}{3}\left(\frac{743}{336}
+ \frac{11}{4}\frac{\mu}{M}\right) x - \frac{32\pi}{5}x^{3/2}\right],
\label{pn-time-freq}
\end{equation}
where $t_c$ is the coalescence time and
\begin{equation}
t_x = 5c^{5/3}(8\pi f)^{-8/3} \left(\frac{GM_\odot}{c^2}\right)^{-5/3}
\left(\frac{\mathcal{M}}{M_\odot}\right)^{-5/3}.
\end{equation}

Now we summarize the time dependence of the source location~\cite{Cutler1998}. The azimuthal angle of the detector in its orbit around the sun is $\bar{\phi}(t_f) = 2\pi t_f/T$ where $T$ is the period of the orbit. Let $\rf{n}$ be the unit vector from the detector to the source and let $\rf{L}$ be the unit vector of the source angular momentum in the ecliptic frame. The azimuthal and polar angles of the angular momentum are $\phi_L$ and $\theta_L$, respectively. If $\rf{z}$ is the unit vector along the $z$ direction, the polar angle of the source in the detector frame is
\begin{equation}
\cos \tilde{\theta}(t_f) = \rf{z}\cdot\rf{n}
= \frac{1}{2}\cos\theta - \frac{\sqrt{3}}{2} \sin\theta \cos(\bar{\phi}(t_f) - \phi),
\label{detframe-polar}
\end{equation}
the azimuthal angle of the source in the detector frame is
\begin{equation}
\tilde{\phi}(t_f) = \bar{\phi}(t_f)
+ \arctan\left[ \frac{\sqrt{3}\cos\theta + \sin\theta \cos(\bar{\phi}(t_f) - \phi)}
{2\sin\theta \sin(\bar{\phi}(t_f) - \phi)}\right],
\label{detframe-azimuthal}
\end{equation}
and the polarization phase of the source in the detector frame is
\begin{equation}
\tan\tilde{\psi}(t_f) = \frac{\rf{L}\cdot\rf{z} - (\rf{L}\cdot\rf{n})(\rf{z}\cdot\rf{n})}
{\rf{n}\cdot(\rf{L}\times\rf{z})}
\label{detframe-polarization}
\end{equation}
where
\begin{align}
\rf{L}\cdot\rf{z} &= \frac{1}{2}\cos\theta_L - \frac{\sqrt{3}}{2} \sin\theta_L \cos(\bar{\phi}(t_f) - \phi_L)\\
\rf{L}\cdot\rf{n} &= \cos\theta_L\cos\theta
+ \sin\theta_L \sin\theta \cos(\phi_L - \phi),
\end{align}
and
\begin{align}
\rf{n}\cdot(\rf{L}\times\rf{z}) &=
\frac{1}{2}\sin\theta_L \sin\theta \sin(\phi_L - \phi) \non\\
&- \frac{\sqrt{3}}{2} \cos\bar{\phi}(t_f)\,
\left( \cos\theta_L \sin\theta \sin\phi - \cos\theta \sin\theta_L \sin\phi_L\right) \non\\
&- \frac{\sqrt{3}}{2} \sin\bar{\phi}(t_f)\,
\left( \cos\theta \sin\theta_L \sin\phi_L - \cos\theta_L \sin\theta \sin\phi \right).
\end{align}
Eqs.~\eqref{detframe-polar}--\eqref{detframe-polarization} are a function of frequency through \eqref{pn-time-freq}.

The antenna patterns as a function of time are given by plugging the detector frame angles \eqref{detframe-polar}--\eqref{detframe-polarization} into \eqref{antenna-patterns}. The amplitude of the waveform in the detector is modulated by
\begin{equation}
\Lambda(f) = \sqrt{[1 + (\rf{L}\cdot\rf{n})^2]^2 F_+^2(f)
+ 4(\rf{L}\cdot\rf{n})^2 F_\times^2(f)}
\end{equation}
as the detector orbits the sun. The time dependence of the antenna pattern also adds the additional polarization phase
\begin{equation}
\tan \phi_p(f) = \frac{2(\rf{L}\cdot\rf{n}) F_\times(f)}
{[1 + (\rf{L}\cdot\rf{n})^2] F_+(f)}
\end{equation}
to the overall phase. Finally, the motion of the detector around the sun Doppler shifts the wave's frequency and adds the additional phase
\begin{equation}
\phi_D(f) = \frac{2\pi f}{c}R \sin\theta \cos(\bar{\phi}(t_f) - \phi)
\end{equation}
where $R$ is the radius of the orbit.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{Chapters/Waveforms/Figures/WaveformAmplitude.pdf}
\caption[Amplitude of combined ground and space network waveform]{Amplitude of the waveform \eqref{full-space-wf} for two equal mass binary systems at a redshift of $z=0.1$. The legend denotes the total mass of the binary system. The time the $80\, M_\odot$ binary stays in the zoomed in region of the plot is approximately 11.7~years. In contrast, the time it takes the $800\,M_\odot$ binary to sweep through the same frequency band (of approximately $35\,\t{mHz}$) is approximately 3~months.}
\label{fig:waveform-amplitude}
\end{figure}

The full waveform projected onto the space detector's time-dependent antenna pattern and accounting for the Doppler shift is
\begin{equation}
h(f) = \frac{\sqrt{3}}{2}\Lambda(f)A_\t{eff}(f)\,
\e^{\i[\Psi_\t{eff}(f) - \phi_D(f) - \phi_p(f)]}.
\label{full-space-wf}
\end{equation}
The factor of $\sin(\pi/3) = \sqrt{3}/2$ accounts for a detector forming an equilateral triangle: the antenna patterns for an interferometer with arms at $60^\circ$ is $\sin(\pi/3)$ times those for an interferometer
 with perpendicular arms.

Finally, note that this projection onto a space detector is most convenient in terms of the source location $\theta$ and $\phi$ in the ecliptic frame and the orientation of the source angular momentum $\theta_L$ and $\phi_L$, while calculations are more commonly done in terms of the inclination $\iota$ and polarization phase $\psi$. The relation between the two parameterizations is given by
\begin{align}
\cos\iota &= \cos\theta_L \sin\delta
+ \sin\theta_L \cos\delta \cos(\phi_L - \alpha)\\
\tan\psi &= \frac{\cos\theta_L + \cos\iota \sin\delta}
{\cos\delta\sin\theta_L\sin(\phi_L - \alpha)}
\end{align}
where $\alpha = \phi$ is the right ascension and $\delta = \pi/2 - \theta$ is the declination of the source.
