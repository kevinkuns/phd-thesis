from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from cbgw.waveforms import Waveform
from cbgw.detector import Detector
import cbgw.binary_params as bp
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes, mark_inset

mpl.rcParams.update({'text.usetex': False,
                     'font.family': 'serif',
                     'font.serif': ['Georgia'],
                     'mathtext.fontset': 'cm',
                     'lines.linewidth': 4,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'axes.labelsize': 'large',
                     'axes.titlesize': 'large',
                     'lines.markersize': 12,
                     'legend.borderpad': 0.2,
                     'legend.fancybox': True,
                     'legend.fontsize': 24,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

savefigs = True


Mc1 = bp.chirp_mass(40, 40)  # chirp mass
Mc2 = bp.chirp_mass(400, 400)
qq = 1  # mass ratio
Dl = 475  # luminosity distance [Mpc] (z = 1)
ra = 0.785  # right ascension [rad]
dec = 0.730  # declination [rad]
thetaL = 0.841  # polar angle of the binary angular momentum [rad]
phiL = 0.785  # azimuthal angle of the binary angular momentum [rad]
phic = 0.000  # coalescence phase [rad]
tc = 0.000  # coalescence time [s]

theta1 = [Mc1, qq, Dl, ra, dec, thetaL, phiL, phic, tc]
theta2 = [Mc2, qq, Dl, ra, dec, thetaL, phiL, phic, tc]

ff = np.logspace(-2, 3, 10000)

det = Detector('Z1')
wf1 = Waveform(theta1, ff, 10)
wf2 = Waveform(theta2, ff, 10)

ffz1, hhz1 = det.projectWaveform(wf1)
ffz2, hhz2 = det.projectWaveform(wf2)

fig = plt.figure(figsize=(12, 9))
ax = fig.gca()
ax.loglog(ffz1, hhz1[0, :], label=r'$M_\mathrm{tot}=80\, M_\odot$')
ax.loglog(ffz2, hhz2[0, :], label=r'$M_\mathrm{tot}=800\, M_\odot$')
ax.legend()
ax.xaxis.grid(True, which='both', alpha=0.5)
ax.xaxis.grid(which='minor', alpha=0.2)
ax.yaxis.grid(True, which='major', alpha=0.5)
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('Strain [1/Hz]')
ax.set_xlim(ffz1[0], ffz1[-1])

axin = zoomed_inset_axes(ax, 3, loc=3)
axin.loglog(ffz1, hhz1[0, :])
axin.loglog(ffz2, hhz2[0, :])
axin.set_xlim(1e-2, 0.0447)
axin.set_ylim(9.25e-20, 1.26e-18)
axin.xaxis.set_major_formatter(plt.NullFormatter())
axin.xaxis.set_minor_formatter(plt.NullFormatter())
axin.yaxis.set_major_formatter(plt.NullFormatter())
axin.yaxis.set_minor_formatter(plt.NullFormatter())
axin.grid(True, which='both', alpha=0.5)
axin.grid(which='minor', alpha=0.2)

mark_inset(ax, axin, loc1=1, loc2=3, alpha=0.4, fc='none')

fig.tight_layout()
if savefigs:
    fig.savefig('Figures/WaveformAmplitude.pdf')
