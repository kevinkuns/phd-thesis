from __future__ import division
import numpy as np
import matplotlib.pyplot as plt


def circ(ff, F, fsr, df):
    # ww = 2*np.pi*(ff + df/2)
    pp = 2*np.pi*(ff + df/2)/fsr
    g = F/np.pi  # gain for critical coupling
    # g = 1
    return g/(1 + 4*F**2/np.pi**2 * np.sin(pp)**2)


ff = np.linspace(-0.25, 0.25, 1000)

cp = circ(ff, 5, 1, 0)
fig = plt.figure()
ax = fig.gca()
ax.plot(ff, cp)
ax.xaxis.set_major_formatter(plt.NullFormatter())
ax.yaxis.set_major_formatter(plt.NullFormatter())

fig.savefig('Figures/circ_power.pdf')
