% \newcommand{\K}{\mathcal{K}}

\chapter{Quantum Noise in Optomechanical Systems}
\label{chap:QNoise}

This chapter very briefly reviews quantum noise in states of the electromagnetic field in Sec.~\ref{sec:quantum-noise-review}. Then Sec.~\ref{sec:optomechanical} introduces the optomechanical interactions that are important in the rest of this thesis and explains their effects on quantum noise. Finally, Sec.~\ref{sec:classical-noise} explains how classical noise degrades quantum states and describes schemes using these optomechanical interactions to ameliorate these effects.

\section{Quantum States of the Electromagnetic Field and Their Noise}
\label{sec:quantum-noise-review}

\subsection{Quantization of the Electromagnetic Field}

In this section we very briefly review the semiclassical quantization of the electromagnetic field. See, for example, Refs.~\cite{Gerry-Knight2005, Mandel-Wolf1995, CohenTannoudji-DupontRoc-Grynberg1989} for more details. All topics in this thesis occur at sufficiently low energies that matter does not need to be quantized.

For simplicity, we consider a single polarization of the electromagnetic field describing a laser beam with an effective cross sectional area $\mathcal{A}$. To quantize the field, one starts by expanding the electromagnetic field in a set of normal modes~\cite{Caves1985, Schumaker1985, Kimble2001, Danilishin2012}
\begin{equation}
E(t) = \int_0^\infty \frac{\d\omega}{2\pi} \sqrt{\frac{2\pi\hbar\omega}{\mathcal{A}c}} a(\omega)\, \e^{-\i\omega t} + \t{h.c.}
\label{one-photon}
\end{equation}
where $a(\omega)$ is the annihilation operator for the mode of frequency $\omega$ which is quantized according to
\begin{equation}
[a(\omega), a^\dag(\omega')] = 2\pi\delta(\omega - \omega'), \qquad
[a(\omega), a(\omega')] = 0.
\label{one-photon-commutators}
\end{equation}
Note that we will always be interested in evaluating fields at a particular point in space and so fields will be written as a function of time only.

For applications to interferometric gravitational wave detectors, it is inconvenient to use \eqref{one-photon} directly. The lasers used in these interferometers have wavelengths of order $1\,\mu\t{m}$ which corresponds to frequencies of order $\omega_0/2\pi = 300\,\t{THz}$. On the other hand, the gravitational waves themselves have frequencies $\Omega/2\pi$ ranging from roughly $10^{-4}~\t{Hz}$ for some space detectors to $10^{4}~\t{Hz}$ for the ground detectors. These signals that we are interested in detecting are imprinted on the light as phase fluctuations on top of the carrier oscillating at $\omega_0$. The detectors do not directly measure signals at $\omega_0$ but instead optically demodulate\footnote{This demodulation is done by combining the signal light with another beam, the local oscillator, before being measured on a photodiode. Appendix.~\ref{chap:homodyne} describes one method of doing this, known as homodyne detection, where the local oscillator is at the same frequency $\omega_0$ as the signal field.} the signals to measure the gravitational wave signals oscillating at $\Omega$. It is thus convenient to have a description of the electromagnetic field in terms of the demodulated signal frequencies which is provided by the two-photon formalism~\cite{Caves1985, Schumaker1985}. One defines the annihilation operators for the upper and lower signal sidebands as
\begin{equation}
a_\pm(\Omega) \equiv a(\omega_0 \pm \Omega) \sqrt{1 \pm \frac{\Omega}{\omega_0}}
\approx a(\omega_0 \pm \Omega).
\label{two-photon-annihilation}
\end{equation}
The only nonzero commutators are
\begin{subequations}
\begin{align}
[a_+(\Omega), a_+^\dag(\Omega)] &= 2\pi\,\delta(\Omega - \Omega') \left(1 + \frac{\Omega}{\omega_0}\right)\approx 2\pi\,\delta(\Omega - \Omega')\\
[a_-(\Omega), a_-^\dag(\Omega)] &= 2\pi\,\delta(\Omega - \Omega') \left(1 - \frac{\Omega}{\omega_0}\right)\approx 2\pi\,\delta(\Omega - \Omega').
\end{align}
\end{subequations}
Since $\Omega/\omega_0 \ll 1$ for our applications, the $\Omega/\omega_0$ terms will be neglected.

In terms of the sideband annihilation operators \eqref{two-photon-annihilation}, the field quantization \eqref{one-photon} becomes
\begin{equation}
E(t) = \sqrt{\frac{2\pi\hbar\omega_0}{\mathcal{A}c}}\,\e^{-\i\omega_0 t}
\int_0^\infty \frac{\d\Omega}{2\pi} \left[ a_+(\Omega)\,\e^{-\i\Omega t}
+ a_-(\Omega)\,\e^{\i\Omega t}\right] + \t{h.c.}
\label{two-photon-quantization}
\end{equation}
It is now useful to define the two-photon modes
\begin{equation}
a_1(\Omega) = \frac{1}{\sqrt{2}}\,[a_+(\Omega) + a_-^\dag(\Omega)], \qquad
a_2(\Omega) = \frac{1}{\sqrt{2}\i}\,[a_+(\Omega) - a_-^\dag(\Omega)],
\label{quadrature-defs}
\end{equation}
whose only nonzero commutators are
\begin{equation}
[a_1(\Omega), a_2^\dag(\Omega)] = - [a_2(\Omega), a_1^\dag(\Omega)]
= 2\pi\i\,\delta(\Omega - \Omega').
\label{quadrature-commutators}
\end{equation}
We can now expand the field \eqref{two-photon-quantization} in terms of the quadrature operators
\begin{equation}
E_k(t) = \sqrt{\frac{4\pi\hbar\omega_0}{\mathcal{A}c}}
\int_0^\infty \frac{\d\Omega}{2\pi}\left[a_k(\Omega)\,\e^{-\i\Omega t} + a_k^\dag(\Omega)\,\e^{\i\Omega t}\right],
\label{quadrature-fields}
\end{equation}
for which the full electromagnetic field becomes
\begin{equation}
E(t) = E_1(t)\cos\omega_0 t + E_2(t)\sin\omega_0 t.
\label{quadrature-expansion}
\end{equation}
Eq.~\eqref{quadrature-expansion} describes the state of a general field of frequency $\omega_0$ with signals oscillating at frequencies of order $\Omega\ll \omega_0$. However, we will often be interested in the case where these signals are oscillating on top of a strong signal of amplitude $E_0$---the carrier. By convention the carrier is usually put in the cosine quadrature. In this case the field is
\begin{equation}
E(t) = \left[E_0 + E_1(t)\right]\cos\omega_0 t + E_2(t)\sin \omega_0 t.
\label{field-expansion}
\end{equation}
For this reason, the cosine quadrature is also often referred to as the amplitude quadrature and the sine quadrature is often referred to as the phase quadrature.

\subsection{Coherent States and Squeezed States}

We now briefly review the quantum noise in commonly used quantum states of the electromagnetic field. The one-sided noise spectral density in the state $\ket{\psi}$ is defined as~\cite{Caves1985, Schumaker1985, Kimble2001}
\begin{equation}
\Braket{\psi}{a_i(\Omega)a_i^\dag(\Omega')}{\psi}_\t{sym} = \frac{1}{2}2\pi\delta(\Omega - \Omega') S_{a_i},
\end{equation}
where
\begin{equation}
\ev{AB}_\t{sym} = \frac{1}{2}\ev{AB + BA}.
\end{equation}
Let $\ket{0_a}$ be the vacuum of the $a$ modes, i.e.\
\begin{equation}
a_+(\Omega)\ket{0_a} = a_-(\Omega)\ket{0_a} = 0.
\end{equation}
From \eqref{quadrature-defs} and \eqref{quadrature-commutators}
\begin{equation}
\Braket{0_a}{a_i(\Omega)a_j^\dag(\Omega')}{0_a} = \frac{1}{2}2\pi\delta(\Omega - \Omega')\delta_{ij}.
\end{equation}
The noise in the vacuum is, therefore,
\begin{equation}
S_{a_1} = S_{a_2} = 1, \qquad S_{a_1a_2} = 0.
\end{equation}
This is the minimum noise allowed by the Heisenberg uncertainty relations. A state for which $S_{a_1}S_{a_2} = 1$ is called a minimum uncertainty state. The $a$ vacuum $\ket{0_a}$ is also called the unsqueezed vacuum or the coherent vacuum. It is useful to visualize quantum states by plotting ellipses that represent the shape of the quantum noise. Fig.~\ref{fig:quantum-noise-ellipses} shows several examples. The horizontal axis represents the noise in the $a_1$, or amplitude, quadrature and the vertical axis represents the noise in the $a_2$, or phase, quadrature. The unsqueezed vacuum is represented by a circle at the origin with radius $S_{a_1} = S_{a_2} = 1$.

\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]{Chapters/QuantumNoise/Figures/noise_ellipses.pdf}
\caption[Quantum noise ellipses]{Quantum noise ellipses}
\label{fig:quantum-noise-ellipses}
\end{figure}

A coherent state is a non-zero minimum uncertainty state. It is obtained by using the one photon displacement operators
\begin{equation}
D(\alpha, a) = \e^{\alpha a^\dag - \alpha^* a}
\end{equation}
to displace the vacuum state:
\begin{equation}
\ket{\alpha_+, \alpha_-} = D(\alpha_+, a_+)D(\alpha_-, a_-)\ket{0_a}
= \e^{\alpha_+ a_+^\dag - \alpha_+^* a_+} \e^{\alpha_- a_-^\dag - \alpha_-^* a_-}\ket{0_a}
\end{equation}
and has non-zero expectation value for the one-photon annihilation operators
\begin{equation}
\Braket{\alpha_+, \alpha_-}{a_\pm}{\alpha_+, \alpha_-} = \alpha_\pm.
\end{equation}
See Refs.~\cite{Caves1985, Schumaker1985} for details and expectation values for the quadrature operators $a_{1,2}$. Physically, a coherent state is a classical signal with the minimum amount of quantum noise. It is the state of an ideal laser, for example. The noise ellipse of a coherent state is again a circle of radius $S_a = 1$, but it is displaced from the origin by the strength of the signal; see Fig.~\ref{fig:quantum-noise-ellipses}. The direction of the displacement is the quadrature of the signal. If the displacement is along the $a_2$ axis, as it is in Fig.~\ref{fig:quantum-noise-ellipses}, it is a phase signal; if it is along the $a_1$ axis it is an amplitude signal. The displacement can be in any direction in which case it is a mixture of the two quadratures.

The uncertainty relations say only that the product of the noise in both quadratures is no less than one. Squeezed states are also minimum-uncertainty states but have the noise in one quadrature less than one at the expense of the noise in the other quadrature being greater than one. The squeezed vacuum state is made by acting on the unsqueezed vacuum with
\begin{equation}
S(r,\phi) = \exp\left[r\left(a_+a_- \e^{-2\i\phi} - a_+^\dag a_-^\dag \e^{2\i\phi}\right)\right].
\end{equation}
The state $S(r, \phi)\ket{0_a}$ has noise $\e^{-2r}$ in the $\phi$ quadrature and $\e^{2r}$ in the $\phi+\pi/2$ quadrature. Again see Refs.~\cite{Caves1985, Schumaker1985} for details. If $\phi=0$ so that the uncertainty is reduced in the amplitude quadrature, the state is an amplitude squeezed state, and if $\phi=\pi/2$ so that the uncertainty is reduced in the phase quadrature, the state is a phase squeezed state. The noise of a squeezed state is plotted as an ellipse with semi-minor axis $\e^{-2r}$ and semi-major axis $\e^{2r}$ with the axis oriented along the direction of squeezing. Squeezed vacuum states can also be displaced. These are signals with reduced uncertainty in one quadrature. Fig.~\ref{fig:quantum-noise-ellipses} shows several examples.

\section{Optomechanical Interactions}
\label{sec:optomechanical}

\subsection{Ponderomotive Squeezing}
\label{sec:ponderomotive}

This thesis is concerned with the generation of squeezing through the optomechanical interaction between a laser and a movable mirror~\cite{Corbitt2006, Chen2013, Aspelmeyer2014}. Intuitively what happens is the following. Suppose we send light towards a movable mirror where it is reflected and returns to the starting point where the phase is measured. Since the light exerts radiation pressure on the mirror, any fluctuations in the amplitude of this force results in fluctuations in the position of the mirror. But the phase of the light also changes when the mirror moves since the distance it has to travel also fluctuates. Therefore, this radiation pressure induced optomechanical interaction converts amplitude fluctuations into phase fluctuations thus correlating the two quadratures and producing a squeezed state. This optomechanical generation of squeezing is known as ponderomotive squeezing. The rest of this section describes this process quantitatively.

Suppose we send the field $E_a(t)$ towards the mirror a distance $L$ from the starting point and measure the field $E_b(t)$ that returns. The two fields are related by $E_b(t) = E_a(t - 2L/c)$. Let $E_a$ have the form \eqref{field-expansion}:
\begin{equation}
E_a(t) = [E_0 + E_{1a}(t)]\cos\omega_0 t + E_{2a}(t)\sin\omega_0 t,
\end{equation}
where $E_0$ is the amplitude of the carrier, and similarly for $E_b$. This notation signifies that $E_a$ is expanded as \eqref{quadrature-fields} with quadrature operators $a_1$ and $a_2$, and $E_b$ is expanded as \eqref{quadrature-fields} with quadrature operators $b_1$ and $b_2$. If the position of the mirror is $L + x(t)$, the relation between $E_a$ and $E_b$ is, for small $x/L$, and $\dot{x}/c$,
\begin{subequations}
\label{mirror-io-td-fields}
\begin{align}
E_b(t) &= [E_0 + E_{1a}(t - 2L/c)] \cos\omega_0t
+ \left[E_{2a}(t-2L/c) + \frac{2\omega_0E_0}{c}\, x(t - L/c)\right]\sin(\omega_0 t) \\
&= E_0\left[ 1 + \frac{E_{1a}(t - 2L/c)}{E_0}\right]
\cos\left[ \omega_0t - \frac{2\omega_0E_0}{c}\, x(t - L/c)\right].
\end{align}
\end{subequations}
Indeed, the vacuum fluctuations $a_1$ are responsible for amplitude fluctuations and the vacuum fluctuations $a_2$ are responsible for phase fluctuations. In the frequency domain, \eqref{mirror-io-td-fields} is
\begin{subequations}
\begin{align}
E_{1b}(\Omega) &= \e^{2\i\beta} E_{1a}(\Omega)\\
E_{2b}(\Omega) &= \e^{2\i\beta} E_{2a} + \e^{\i\beta}\frac{2\omega_0E_0}{c}\, x(\Omega),
\end{align}
\label{mirror-io-fd-fields}
\end{subequations}
where $\beta = \Omega L/c$ is the phase gained going one way.

Now we relate the mirror position $x$ to the amplitude vacuum fluctuations $E_{1a}$. The radiation pressure induced force on the mirror is
\begin{equation}
F = \frac{2P}{c}
= 2\frac{\mathcal{A}c}{8\pi}\left[E_0 + E_{1a}(t - L/c)\right]^2
= \frac{\mathcal{A}E_0^2}{4\pi}
+ \frac{\mathcal{A}E_0 E_{1a}(t - L/c)}{2\pi}
\equiv F_0 + F_\t{BA}.
\end{equation}
The first term $F_0$ is the DC contribution to the radiation pressure force which would be canceled by a control system. In gravitational wave detectors, the mirrors are suspended from pendula which cancel this force at DC. Above the mechanical resonance frequency of the pendula (roughly 1~Hz in the case of these detectors) the mirrors behave as free masses like we are considering here. The second term $F_\t{BA}$ is the back action force due to the amplitude vacuum fluctuations $a_1$. The equations of motion for a free mass in the presence of this radiation pressure force are
\begin{equation}
-M\Omega^2\, x(\Omega) = \e^{\i\beta}\, F_\t{BA}(\Omega)
= \e^{\i\beta}\frac{\mathcal{A}E_0}{2\pi} E_{1a}(\Omega),
\end{equation}
where $M$ is the mass of the mirror.

Plugging this into \eqref{mirror-io-fd-fields} and using \eqref{quadrature-fields} to write it in terms of the quadrature operators, the input output relations for reflection from a mirror are
\begin{subequations}
\begin{align}
b_1 &= \e^{2\i\beta}a_1\\
b_2 &= \e^{2\i\beta}(a_2 - \K a_1),
\end{align}
\label{mirror-io}
\end{subequations}
where
\begin{equation}
\K = \frac{8 P \omega_0}{M\Omega^2 c^2}
\label{mirror-coupling}
\end{equation}
is the strength of the optomechanical coupling and $P$ is the power of the light. The noise spectral densities are
\begin{equation}
S_{b_1} = 1, \qquad S_{b_2} = 1 + \K^2, \qquad S_{b_1b_2} = -\K
\label{mirror-noise}
\end{equation}
This is a squeezed state with $r = \arcsinh(\K/2)$. Even though this is a very simple system, it captures the fundamental physics of ponderomotive squeezing. The structure of \eqref{mirror-io} and \eqref{mirror-noise} is very general; it is mostly the optomechanical coupling $\K$ that changes for different systems. Eq.~\eqref{mirror-io} says that amplitude fluctuations cause amplitude fluctuations and phase fluctuations cause phase fluctuations. But amplitude fluctuations also cause phase fluctuations with strength $\K$. Furthermore, the first term in \eqref{mirror-noise} for the noise for the phase quadrature is the shot noise $S_\t{shot} = 1$, and the second term is the radiation pressure noise $S_\t{RP} = \K^2$. The origin of both comes from the vacuum fluctuations beating with the carrier: the beating of the phase fluctuations $a_2$ cause the shot noise and the beating of the amplitude fluctuations $a_1$ cause the radiation pressure noise.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{Chapters/QuantumNoise/Figures/ellipses_combined.pdf}
\caption[Quantum noise ellipses ponderomotively generated as a function of frequency]{Quantum noise ellipses ponderomotively generated as a function of frequency. Frequency increases from left to right as the amount of squeezing is reduced.}
\label{fig:freq-ellipse}
\end{figure}

Note that the optomechanical coupling from \eqref{mirror-coupling} is proportional to the power $P$ and inversely proportional to the mass $M$ and squared frequency $\Omega^2$. This is also a general feature of ponderomotive squeezing in more complicated systems. The frequency dependence may be different but it will eventually decrease at higher frequencies. Note that this means the squeezing is frequency dependent and has a larger magnitude at lower frequencies. Fig.~\ref{fig:freq-ellipse} shows the quantum noise ellipses for a ponderomotively squeezed state for increasing frequency. Indeed, the magnitude of the squeezing decreases and the ellipse rotates counterclockwise as the frequency increases.

It is difficult to plot these ellipses when the magnitude of the squeezing becomes large. For this reason we also plot them in terms of decibels relative to unsqueezed vacuum noise (dBvac), i.e.\ $10\log_{10}(S_b/S_a)$. Fig.~\ref{fig:freq-ellipse-dB} shows these noise ellipses for the same noise ellipses plotted in Fig.~\ref{fig:freq-ellipse}. The radial direction is measured in dBvac. The red dashed circle is the unsqueezed vacuum ellipse and is at 0~dBvac. We will use these plots exclusively in Chap.~\ref{chap:40m} since the magnitude of squeezing can be as much as 50~dBvac.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{Chapters/QuantumNoise/Figures/log_ellipses_combined.pdf}
\caption[Quantum noise ellipses expressed as decibels relative to vacuum noise]{Quantum noise ellipses expressed as decibels relative to vacuum noise. The same ellipses are plotted here as are plotted in Fig.~\ref{fig:freq-ellipse}. The radial distance is measured in dBvac and the red dashed circle is the noise of the unsqueezed vacuum at 0~dBvac.}
\label{fig:freq-ellipse-dB}
\end{figure}

The input output relations for a Michelson interferometer with Fabry-Perot cavities as arms (FPMI) also has the form \eqref{mirror-io}. In fact, the response to differential arm motion of such an interferometer is equivalent to that of a single Fabry-Perot cavity.\footnote{The same is true for a power recycled Fabry-Perot Michelson interferometer (PRFPMI). For the purposes of differential arm motion, the only difference is the relationship between the amount of power incident on the interferometer and the amount of power in the arms.} We will discuss this in detail in Chap.~\ref{chap:40m} where we also include the signal recycling mirror. In that case, the interferometer acts like a three mirror cavity; see Fig.~\ref{fig:coupled-cavities}. For now we stick with the FPMI, or PRFPMI, which is equivalent to a single Fabry-Perot cavity. The input output relations are~\cite{Kimble2001}
\begin{equation}
\bbmat
b_1\\
b_2
\ebmat = 
\e^{2\i\beta} \bbmat
1 & 0\\
-\K & 1 \ebmat
\bbmat
a_1\\
a_2
\ebmat +
\frac{\sqrt{2\K}}{h_\t{SQL}L}
\bbmat
0\\
1\ebmat \Delta L,
\label{prfpmi-io}
\end{equation}
where $L$ is the length of the arms, the strength of the optomechanical coupling is
\begin{equation}
\K = \frac{2P_\t{BS}/P_\t{SQL}}{(\Omega/\gamma)^2[1 + (\Omega/\gamma)^2]}
\label{coupling-prfpmi}
\end{equation}
$\gamma= cT_i/4L$ is the cavity pole, $\beta = \arctan\Omega/\gamma$, $P_\t{BS}$ is the power on the beam splitter, and
\begin{equation}
h_\t{SQL} = \sqrt{\frac{8\hbar}{M\Omega^2L^2}}, \qquad
P_\t{SQL} = \frac{ML^2\gamma^4}{4\omega_0}.
\end{equation}
We will explore the behavior of Fabry-Perot cavities (and thus interferometers) in detail in Chap.~\ref{chap:40m}, but for now notice the form of the optomechanical coupling \eqref{coupling-prfpmi}. As with the free mass $\K$, it is proportional to the power $P_\t{BS}$ and inversely proportional to the mass $M$. For $\Omega < \gamma$, the coupling also has the behavior of the free mass coupling falling like $\Omega^{-2}$; for $\Omega > \gamma$, it falls faster like $\Omega^{-4}$.

\subsection{Optical Spring}

When a Fabry-Perot cavity is detuned from resonance, the radiation pressure force produces a spring-like coupling between the two mirrors known as an optical spring~\cite{Chen2013, Aspelmeyer2014, Buonanno2002}. Fig.~\ref{fig:optical-spring} shows a Fabry-Perot cavity with one movable mirror suspended from a pendulum as well as the circulating power in the cavity as a function of mirror position. The force between the mirrors is due to the radiation pressure of the circulating power and is proportional to $P_\t{circ}$. When the cavity is held on resonance, the circulating power $P_\t{circ}$ is maximized and there is no first order change in the power as the position of the mirror moves $\pd P_\t{circ}/\pd x = 0$.

When the length of the cavity is made slightly longer, also known as blue-detuning, the circulating power is reduced. Now, if the mirror moves slightly to the right, the circulating power is reduced, which reduces the radiation pressure, which then forces the mirror back towards the left until the radiation pressure force balances the restoring force from the pendulum. Similarly, if the mirror moves to the left the circulating power increases which pushes the mirror back to the right. The detuning has created an optical spring between the two mirrors with spring constant proportional to $- \pd P_\t{circ}/\pd x > 0$.

\begin{figure}
\centering
\includegraphics[scale=1]{Chapters/QuantumNoise/Figures/spring.pdf}
\caption{Optical spring}
\label{fig:optical-spring}
\end{figure}

If the length of the cavity is made slightly shorter, also known as red-detuning, the circulating power is again reduced. Now, however the situation is the opposite as with the blue-detuned case. Motion to the right increases the circulating power and forces the mirror further to the right; motion to the left decreases the circulating power and forces the mirror further to the left. This detuning has created an optical anti-spring with spring constant proportional to $-\pd P_\t{circ}/\pd x < 0$.

Note that the force slightly lags the position of the mirror. This leads to the spring being anti-damped by this phase delay and the anti-spring being damped. Thus, optical springs or anti-springs are always unstable: the spring is statically stable and dynamically unstable, but the anti-spring is statically unstable and dynamically stable. Thus any use of an optical spring in an experiment requires a control system to stabilize.\footnote{Ref.~\cite{Rehbein2008} suggests a scheme where two optical springs can be combined to produce a stable composite system.}

\section{The Effects of Classical Noise on Quantum States}
\label{sec:classical-noise}

Anytime there are losses, unsqueezed vacuum is added in quadrature with the signal thus degrading the squeezing. This can be thought of as circularizing the quantum noise ellipse; see Fig.~\ref{fig:classical-noise}. If $a$ is the input signal before losses and $b$ is the output signal after $\epsilon$ losses, the resulting state is
\begin{equation}
b = \sqrt{1 - \epsilon}\, a + \sqrt{\epsilon}\, c
\end{equation}
where $c$ is unsqueezed vacuum. Since $c$ is, by definition, uncorrelated with $a$ and, since $S_c=1$, the noise becomes
\begin{equation}
S_b = (1 - \epsilon)S_a + \epsilon.
\end{equation}
If $a$ was squeezed so that $S_a < 1$, the unsqueezed vacuum has degraded the squeezing. In gravitational wave detectors, and elsewhere, much effort may be made to squeeze the light so that a small signal can be detected. The presence of classical noise seriously limits the benefit of such efforts. Even if the signal is not appreciably reduced by losses or noise, the quantum noise can become significantly worse as is shown in Fig.~\ref{fig:classical-noise}.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{Chapters/QuantumNoise/Figures/ClassicalNoise.pdf}
\caption[Degradation of a squeezed state by classical noise]{Degradation of a squeezed state by classical noise. A signal with 12~dB phase squeezing in the phase quadrature encounters $10\%$ losses, which mixes in $10\%$ unqueezed vacuum. The arrows denote the signal.}
\label{fig:classical-noise}
\end{figure}

An interferometric gravitational wave detector can have significant squeezing inside the interferometer, but then encounter losses in the readout chain between the interferometer and the photodetectors. To ameliorate these effects, one can consider amplifying the signal and the noise before encountering losses that could reduce the signal below detectable levels, and this should be done as soon as possible after the signal exits the interferometer. This is similar to the idea of a classical amplifier where both the signal and noise are amplified but the signal to noise ratio stays the same, if it is an ideal amplifier. In reality the amplifier will add some noise itself.

When dealing with very weak signals near the quantum limit, however, a more careful analysis is required. Furthermore, we want the output signal to be in a highly quantum squeezed state with reduced uncertainty in the signal quadrature. In addition to degrading the signal itself, any noise added to such a state decreases the squeezing and thus also decreases the SNR. So the amplification is extra important here.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{Chapters/QuantumNoise/Figures/Amplification10dB.pdf}
\caption[Amplification of a squeezed state before degradation by classical noise]{Amplification of a squeezed state before degradation by classical noise. The same original 12~dB squeezed state from Fig.~\ref{fig:classical-noise} is shown as the blue dashed ellipse. The orange dashed ellipse is the noise that would be found with no amplification. The blue solid ellipse is the input state amplified by 10~dB, and the state after $10\%$ losses is shown as the solid orange ellipse.}
\label{fig:psoma}
\end{figure}

To prevent the amplifier itself from adding extra quantum noise to the signal, a phase insensitive amplifier which amplifies all quadratures equally cannot be used, however. Such phase insensitive amplifiers must add at least half a quantum of vacuum noise to the signal~\cite{Caves1982, Caves2012, Pandey2013, Combes2016}. This can be understood physically by noting that some other mode must be present to provide the energy for the amplification. It is the quantum noise from this mode that is added to the signal. This additional quantum noise can be avoided, however, if a phase sensitive amplifier is used which amplifies the quadratures differently. If the signal quadrature is amplified by a factor of $\e^r$ then the orthogonal quadrature is reduced by the same amount. Thus, if the signal is in the $a_2$ quadrature, the amplifier should output the state
\begin{equation}
b_1 = \e^{-r} a_1, \qquad b_2 = \e^{r} a_2.
\end{equation}
Such a phase sensitive amplification scheme is shown in Fig.~\ref{fig:psoma}.

It is possible to make such phase sensitive amplifiers out of optomechanical systems using ponderomotive squeezing. Such amplifiers are known as phase-sensitive optomechanical amplifiers (PSOMAs). The general amplifier scheme as used in interferometers is shown in Fig.~\ref{fig:psoma-overview}. The input and output from the main interferometer are the fields $a_\t{IFO}$ and $b_\t{IFO}$, respectively. These signals are sent through an amplifier which has its own input and output fields $\alpha$ and $\beta$. (The amplifier fields $\alpha$ and $\beta$ would be responsible for the extra quantum noise if the amplifier was phase insensitive.) The full input and output from the detector are the fields $a$ and $b$. So the output from the interferometer $b_\t{IFO}$ is amplified to the detected signal $b$ and input vacuum $a$ enters the interferometer as $a_\t{IFO}$.

\begin{figure}
\centering
\includegraphics[height=0.4\textheight]{Chapters/QuantumNoise/Figures/AmplifierOverviewFig.pdf}
\caption{General scheme for phase sensitive optomechanical amplifier}
\label{fig:psoma-overview}
\end{figure}

Such PSOMAs will have the general form \eqref{mirror-io}. The signal to be amplified should be put in the amplitude quadrature. The general interaction \eqref{mirror-io} will than amplify the signal into the phase quadrature. Explicitly, suppose the signal exits the interferometer in the amplitude quadrature $b_\t{IFO,1}$. The amplifier should then amplify the signal like
\begin{equation}
\bbmat
b_1\\
b_2
\ebmat = 
\bbmat
1 & 0\\
-\K & 1\ebmat
\bbmat
b_{\t{IFO},1}\\
b_{\t{IFO},2}
\ebmat
\end{equation}
The signal that exits the interferometer as $b_\t{IFO,1}$ gets amplified by $\K$ and should be measured in the output phase quadrature $b_2$.

The form of $\K$ will be determined by the details of the amplifier. The amplifier field $\alpha$ is responsible for the amplification and one of the $\alpha$ quadratures will have the strong carrier, or pump. Note that the quadrature is defined by where the carrier or pump is. So even if a signal is in the phase quadrature when it exits the interferometer $b_\t{IFO,2}$ it can be in the amplitude quadrature with respect to the amplifier by making the amplifier pump field $\alpha_2$ instead of $\alpha_1$. The important thing is that the signal that exits the interferometer be in the same quadrature as the amplifier pump. Whichever quadrature that is gets amplified by the optomechanical interaction into the other quadrature where it should be measured.

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{Chapters/QuantumNoise/Figures/IFO.pdf}
\caption[Using an interferometer as a phase sensitive optomechanical amplifier]{Using an interferometer as a phase sensitive optomechanical amplifier. The Faraday isolator acts as an optical circulator.}
\label{fig:IFO}
\end{figure}

One way of realizing such an optomechanical amplifier in practice is with another interferometer, as is illustrated in Fig.~\ref{fig:IFO}. The details of this kind of interferometer are given by \eqref{prfpmi-io}. A Faraday isolator serves as an optical circulator and is used to send the signal from the main interferometer $b_\t{IFO}$ into the dark port of the amplifier interferometer. The amplified signal $b$ exits the dark port and is detected. The noise entering the full detector $a$ is routed through the Faraday into the main interferometer and bypasses the amplifying interferometer. That the vacuum entering the main interferometer bypasses the amplifier is important so that the amplifier doesn't make the quantum noise of the main interferometer itself worse. In this case, the amplifier pump field $\alpha$ is the laser of the amplifier interferometer.

Another way of realizing such an optomechanical amplifier is to use a ring cavity, as is illustrated in Fig.~\ref{fig:ring}. A strong pump field is sent into the ring and travels in the same direction as the signal field $b_\t{IFO}$ exiting from the interferometer. This amplifies the output signal $b$ which is detected. The noise $a$ entering the detector travels in the opposite direction around the ring and so the noise $a_\t{IFO}$ entering the interferometer is not amplified. The ring thus acts as an amplifer and a circulator. There are also two sets of modes that couple into the amplifier in this case denoted by $\alpha, \beta, \tilde{\alpha}$, and $\tilde{\beta}$. If the input noise $a$ is squeezed then the noise $\tilde{\alpha}$ coupled in through the amplifier will degrade the resulting squeezing $a_\t{IFO}$ entering the dark port of the interferometer.

\begin{figure}
\centering
\includegraphics[height=0.5\textheight]{Chapters/QuantumNoise/Figures/RingCavity.pdf}
\caption[Using a ring cavity as a phase sensitive optomechanical amplifier]{Using a ring cavity as a phase sensitive optomechanical amplifier. The one-way direction of the pump makes turns the ring cavity into an optical ciculator.}
\label{fig:ring}
\end{figure}
