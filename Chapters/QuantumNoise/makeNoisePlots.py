from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib as mpl
from matplotlib.patches import Ellipse
import subprocess

plt.ion()

savefigs = True

if savefigs:
    mpl.rcParams.update({'text.usetex': True,
                         'font.family': 'serif',
                         # 'font.serif': 'Georgia',
                         # 'mathtext.fontset': 'cm',
                         'lines.linewidth': 2.5,
                         'font.size': 16,
                         'xtick.labelsize': 'medium',
                         'ytick.labelsize': 'medium',
                         'legend.fancybox': True,
                         'legend.fontsize': 12,
                         'legend.framealpha': 0.7,
                         'legend.handletextpad': 0.5,
                         'legend.labelspacing': 0.2,
                         'legend.loc': 'best',
                         'savefig.dpi': 80,
                         'pdf.compression': 9})
else:
    mpl.rcParams.update({'text.usetex': False,
                         'font.family': 'serif',
                         'font.serif': 'Georgia',
                         'mathtext.fontset': 'cm',
                         'lines.linewidth': 2,
                         'font.size': 16,
                         'legend.loc': 'best',
                         'savefig.dpi': 80,
                         'pdf.compression': 9})


def mag2db(g):
    return 20*np.log10(g)


def db2scale(db):
    return 10**(db/20)


def db2sqz(db):
    return np.log(db2scale(db))


def plotArrow(fig, length, col, alpha=1):
    fig.gca().arrow(0, 0, 0, length, length_includes_head=True,
                    head_width=0.1, color=col, alpha=alpha)


def plotNoise(fig, sig, r_sig, eps, ls, alpha=1):
    # bb1 = np.exp(r_sig)*aa1
    # bb2 = np.exp(-r_sig)*aa2
    # cc1 = np.sqrt(1 - eps)*bb1 + np.sqrt(eps)*aa1
    # cc2 = np.sqrt(1 - eps)*bb2 + np.sqrt(eps)*aa2
    # fig.gca().plot(bb1, bb2 + sig, 'C0', linestyle=ls, alpha=alpha)
    # fig.gca().plot(cc1, cc2 + np.sqrt(1 - eps)*sig, 'C1',
    #                linestyle=ls, alpha=alpha)
    Sb1 = np.exp(2*r_sig)
    Sb2 = np.exp(-2*r_sig)
    Sc1 = np.sqrt((1 - eps)*Sb1 + eps)
    Sc2 = np.sqrt((1 - eps)*Sb2 + eps)
    bsig = sig  # np.exp(r_sig)*sig
    csig = np.sqrt(1 - eps)*bsig
    ell1 = Ellipse(
        xy=[0, bsig], width=2*np.sqrt(Sb1), height=2*np.sqrt(Sb2),
        fill=False, color='C0', alpha=alpha, linestyle=ls)
    ell2 = Ellipse(
        xy=[0, csig], width=2*np.sqrt(Sc1), height=2*np.sqrt(Sc2),
        fill=False, color='C1', alpha=alpha, linestyle=ls)
    fig.gca().add_artist(ell1)
    fig.gca().add_artist(ell2)
    plotArrow(fig, bsig, 'C0', alpha=alpha)
    plotArrow(fig, csig, 'C1', alpha=alpha)


def plotAll(sig, r_sig, r_amp, eps):
    fig = plt.figure()
    fig.gca().plot(aa1, aa2, 'C2-', alpha=0.3)
    fig.gca().axvline(0, color='k', alpha=0.2)
    fig.gca().axhline(0, color='k', alpha=0.2)
    fig.gca().set_aspect('equal')
    plotNoise(fig, np.exp(r_amp)*sig, r_sig-r_amp, eps, '-')
    plotNoise(fig, sig, r_sig, eps, '-.', alpha=0.7)
    fig.gca().legend()
    l_raw_sig = mlines.Line2D([], [], color='k', linestyle='-.', alpha=0.7)
    l_amp_sig = mlines.Line2D([], [], color='k', linestyle='-')
    lsig = mlines.Line2D([], [], color='C0')
    lnoise = mlines.Line2D([], [], color='C1')
    lvac = mlines.Line2D([], [], color='C2', alpha=0.3)
    handles = [lsig, lnoise, l_raw_sig, l_amp_sig, lvac]
    labels = ['sig', 'sig+noise', 'input sig', 'amplified sig',
              'unsqzed vac']
    fig.gca().legend(handles, labels, loc='upper left')
    return fig


tt = 2*np.pi*np.linspace(0, 1, 200)
aa1 = np.cos(tt)
aa2 = np.sin(tt)
sig = 1
eps = 0.1
r_sig = db2sqz(12)
r_amp = db2sqz(10)
bb1 = np.exp(r_sig)*aa1
bb2 = np.exp(-r_sig)*aa2
cc1 = np.sqrt(1 - eps) * bb1 + np.sqrt(eps) * aa1
cc2 = np.sqrt(1 - eps) * bb2 + np.sqrt(eps) * aa2
bb1_amp = np.exp(-r_amp)*bb1
bb2_amp = np.exp(r_amp)*bb2
cc1_amp = np.sqrt(1 - eps) * bb1_amp + np.sqrt(eps)*aa1
cc2_amp = np.sqrt(1 - eps) * bb2_amp + np.sqrt(eps)*aa2


# fig = plt.figure()
# fig.gca().plot(aa1, aa2 + sig)
# fig.gca().plot(bb1, bb2 + sig)
# plotArrow(fig, sig, 'C1')
# # fig.gca().arrow(0, 0, 0, sig, length_includes_head=True, head_width=0.1,
# #                 color='C1')
# plotArrow(fig, np.sqrt(1 - eps)*sig, 'C2')
# fig.gca().plot(cc1, cc2 + np.sqrt(1 - eps)*sig)
# fig.gca().plot(bb1_amp, bb2_amp + np.exp(r_amp)*sig, 'C1-')
# fig.gca().plot(cc1_amp, cc2_amp + np.sqrt(1 - eps)*np.exp(r_amp)*sig, 'C2-')
# plotArrow(fig, np.exp(r_amp)*sig, 'C1')
# plotArrow(fig, np.sqrt(1 - eps)*np.exp(r_amp)*sig, 'C2')
# fig.gca().axvline(0, color='k', alpha=0.2)
# fig.gca().axhline(0, color='k', alpha=0.2)
# fig.gca().set_aspect('equal')
# fig.gca().set_ylim(-1.5, 4.5)


fig0 = plt.figure()
fig0.gca().plot(aa1, aa2, 'C2-', alpha=0.3)
fig0.gca().axvline(0, color='k', alpha=0.2)
fig0.gca().axhline(0, color='k', alpha=0.2)
fig0.gca().set_aspect('equal')
plotNoise(fig0, sig, r_sig, eps, '-')
lsig = mlines.Line2D([], [], color='C0')
lnoise = mlines.Line2D([], [], color='C1')
lvac = mlines.Line2D([], [], color='C2', alpha=0.3)
handles = [lsig, lnoise, lvac]
labels = ['sig', 'sig+noise', 'unsqzed vac']
fig0.gca().legend(handles, labels, loc='lower left')
# fig0.gca().set_title('12 dB input squeezing')


# fig2 = plt.figure()
# fig2.gca().plot(aa1, aa2, 'C2-', alpha=0.3)
# fig2.gca().axvline(0, color='k', alpha=0.2)
# fig2.gca().axhline(0, color='k', alpha=0.2)
# fig2.gca().set_aspect('equal')
# plotNoise(fig2, np.exp(r_amp)*sig, r_sig-r_amp, eps, '-')
# plotNoise(fig2, sig, r_sig, eps, '-.', alpha=0.7)
# fig2.gca().legend()
# l_raw_sig = mlines.Line2D([], [], color='k', linestyle='-.', alpha=0.7)
# l_amp_sig = mlines.Line2D([], [], color='k', linestyle='-')
# lsig = mlines.Line2D([], [], color='C0')
# lnoise = mlines.Line2D([], [], color='C1')
# lvac = mlines.Line2D([], [], color='C2', alpha=0.3)
# handles = [lsig, lnoise, l_raw_sig, l_amp_sig, lvac]
# labels = ['sig', 'sig+noise', 'input sig', 'amplified sig',
#           'unsqzed vac']
# fig2.gca().legend(handles, labels, loc='upper left')


fig12 = plotAll(sig, db2sqz(12), db2sqz(10), eps)
# fig12.gca().set_title('12 dB input squeezing, 10 dB amplification')
# xlims = fig12.gca().get_xlim()

# fig10 = plotAll(sig, db2sqz(10), db2sqz(10), eps)
# fig10.gca().set_title('10 dB input squeezing, 10 dB amplification')
# #fig10.gca().set_xlim(xlims)

# fig8 = plotAll(sig, db2sqz(8), db2sqz(10), eps)
# fig8.gca().set_title('8 dB input squeezing, 10 dB amplification')
# #fig8.gca().set_xlim(xlims)
# #ylims = fig8.gca().get_ylim()

# #fig12.gca().set_ylim(ylims)
# #fig10.gca().set_ylim(ylims)

xlims = [-4.2, 4.2]
ylims = [-1.2, 4.8]

# for figi in [fig, fig12, fig10, fig8]:
#     figi.gca().set_xlim(xlims)
#     figi.gca().set_ylim(ylims)
#     figi.tight_layout()

fig12.gca().set_ylim(ylims)
fig12.gca().set_xlim(xlims)
fig0.gca().set_ylim([-1.2, 2])
fig0.gca().set_xlim(xlims)

fig0.tight_layout()
fig12.tight_layout()

if savefigs:
    fnames = ['ClassicalNoise', 'Amplification10dB']
    figs = [fig0, fig12]
    for fname, fig in zip(fnames, figs):
        sname = 'Figures/{:s}.pdf'.format(fname)
        fig.savefig(sname)
        cmd = ['pdfcrop', sname, sname]
        subprocess.call(cmd)

# fig10.tight_layout()
# fig8.tight_layout()


# fig.savefig('noise.pdf')
# fig12.savefig('sig12.pdf')
# fig10.savefig('sig10.pdf')
# fig8.savefig('sig8.pdf')
