from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from pycbc.waveform import get_fd_waveform

plt.ion()

mpl.rcParams.update({'text.usetex': False,
                     'font.family': 'serif',
                     'font.serif': 'Georgia',
                     'mathtext.fontset': 'stix'})


hh, _ = get_fd_waveform(approximant='IMRPhenomD', mass1=1.4, mass2=1.4,
                        f_lower=10, delta_f=10)

inds = hh != 0
ff = np.array(hh.sample_frequencies)[inds]
hh = np.array(hh.data)[inds]
savefigs = True


def H(ff):
    return 1/(1 + 1j * ff/250)


def axes_off(ax):
    ax.yaxis.set_minor_locator(plt.NullLocator())
    ax.yaxis.set_major_locator(plt.NullLocator())
    ax.xaxis.set_minor_locator(plt.NullLocator())
    ax.xaxis.set_major_locator(plt.NullLocator())
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)


ss = H(ff)*hh

fig = plt.figure()
gs = gridspec.GridSpec(2, 2, hspace=0.05, wspace=0.05)

# fig1 = plt.figure()
# ax1 = fig1.gca()
ax1 = fig.add_subplot(gs[0])
ax1.loglog(ff, np.abs(hh), c='xkcd:cerulean', label=r'$h(f)$')
# ax1.loglog(ff, np.abs(ss), label=r'$s(f)$')
axes_off(ax1)
ax1.legend(frameon=False, framealpha=0, handlelength=1)
# if savefigs:
#     fig1.savefig('1.pdf')


# fig2 = plt.figure()
# ax2 = fig2.gca()
ax2 = fig.add_subplot(gs[2])
ax2.loglog(ff, np.abs(H(ff)), c='xkcd:emerald',
           label=r'$H_\mathrm{det}(f)$')
ax2.loglog(ff, np.ones_like(ff), c='xkcd:emerald', ls=':', alpha=0.2)
ax2.set_xlabel(r'$f$')
axes_off(ax2)
ax2.legend(frameon=False, framealpha=0, handlelength=1)
# if savefigs:
#     fig2.savefig('2.pdf')


# fig3 = plt.figure()
# ax3 = fig3.gca()
ax3 = fig.add_subplot(gs[3])
ax3.loglog(ff, np.ones_like(ff), c='xkcd:eggplant purple',
           label=r'$S_a^{1/2}$')
ax3.loglog(ff, 1/np.abs(H(ff)), c='xkcd:dusty orange',
           label=r'$S_\mathrm{shot}^{1/2}$')
ax3.set_xlabel(r'$f$')
axes_off(ax3)
ax3.legend(frameon=False, framealpha=0, handlelength=1, labelspacing=0.05)
ax3.set_ylim(0.2, 100)
# if savefigs:
#     fig2.savefig('3.pdf')


# fig4 = plt.figure()
# ax4 = fig4.gca()
ax4 = fig.add_subplot(gs[1])
ax4.loglog(ff, np.abs(ss), c='xkcd:scarlet', label=r'$s(f)$')
ax4.loglog(ff, np.abs(hh), c='xkcd:scarlet', ls=':', alpha=0.2)
axes_off(ax4)
ax4.legend(frameon=False, framealpha=0, handlelength=1)
# if savefigs:
#     fig4.savefig('4.pdf')

gs.tight_layout(fig)
dpi = fig.get_dpi()
fig.set_size_inches(295/dpi, 200/dpi)
if savefigs:
    fig.savefig('detector_plots.pdf')
