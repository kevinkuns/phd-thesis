from __future__ import division
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.constants as scc


mpl.rcParams.update({'text.usetex': False,
                     'font.family': 'serif',
                     'font.serif': ['Georgia'],
                     'mathtext.fontset': 'cm',
                     'lines.linewidth': 4,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'axes.labelsize': 'large',
                     'axes.titlesize': 'large',
                     'lines.markersize': 12,
                     'legend.borderpad': 0.2,
                     'legend.fancybox': True,
                     'legend.fontsize': 24,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

plt.ion()

savefigs = False

Tprm = 0.01
Tsrm = 0.25
Titm = 0.01384
Tetm = 13.7e-6
Litm = 20e-6
ti = np.sqrt(Titm)
ri = np.sqrt(1 - Titm - Litm)
re = np.sqrt(1 - Tetm)
ts = np.sqrt(Tsrm)
tp = np.sqrt(Tprm)
rs = np.sqrt(1 - Tsrm)
rp = np.sqrt(1 - Tprm)

lambda0 = 1064e-9
omega0 = 2*np.pi*scc.c/lambda0
Larm = 37.795
Lsrc = 4.044


f1 = 11.066209e6
f2 = 5*f1


def armReflectivity(fi):
    """Arm reflectivity for sideband frequency fi
    """
    phi = 2*np.pi*fi*Larm/scc.c
    num = -ri + re*(ri**2 + ti**2)*np.exp(-2j*phi)
    den = 1 - ri*re*np.exp(-2j*phi)
    return num/den


def amplitudeTransmission(fi, lsch):
    phim = 2*np.pi*fi*lsch/(2*scc.c)
    phis = 2*np.pi*fi*Lsrc/scc.c
    rarm = armReflectivity(fi)
    rabs = np.abs(rarm)
    rph = np.angle(rarm)
    eip = np.exp(1j*(rph - 2*phis))
    num = tp*ts*rarm*np.sin(2*phim)
    den = 1 - rabs * (rp + rs*eip) * np.cos(2*phim) + eip*rp*rs*rabs**2
    return num/den


lsch = np.linspace(0, 0.2, 1000)  # Schnupp asymmetry [m]
T1 = np.abs(amplitudeTransmission(f1, lsch))**2
T2 = np.abs(amplitudeTransmission(f2, lsch))**2


fig = plt.figure(figsize=(12, 9))
ax = fig.gca()
ax.semilogy(lsch*1e3, T1, label=r'$f_1=11\,\mathrm{MHz}$')
ax.semilogy(lsch*1e3, T2, label=r'$f_2=55\,\mathrm{MHz}$')
ax.legend(loc='lower right')
ax.set_xlim(lsch[0], lsch[-1]*1e3)
ax.set_xlabel('Schnupp asymmetry [mm]')
ax.set_ylabel('Power transmissivity')
ax.set_title('RF Sideband Transmission to Antisymmetric Port')
ax.grid(True, which='major', alpha=0.5)
ax.yaxis.set_minor_locator(plt.NullLocator())
ax.grid(which='minor', alpha=0.2)
fig.tight_layout()

if savefigs:
    fig.savefig('Figures/sideband_transmission.pdf')
