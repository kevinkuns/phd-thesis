\chapter{Recycling Cavity Lengths}
\label{chap:recycling-lengths}

A dual-recycled Fabry-Perot Michelson interferometer (see~Fig.\ref{fig:DRFPMI}), such as the 40~m interferometer or LIGO, have five length degrees of freedom: the common and differential arm lengths (CARM and DARM), the power recycling cavity length (PRCL), the signal recycling cavity length (SRCL), and the Michelson length (MICH) which is the difference in the distance between the two ITMs and the beam splitter. To simultaneously control these lengths, two sets of RF phase sidebands are added to the main laser before it enters the interferometer. These are known as the $f_1$ and $f_2 = 5f_1$ sidebands. At the 40~m, $f_1 = 11\,\t{MHz}$ and $f_2=55\,\t{MHz}$. These sidebands have different resonance conditions in the different interferometer cavities which enables one to monitor the lengths of the cavities. See, for example, Ref.~\cite{Staley2014} for a description of how these sidebands are used to sense and control these five degrees of freedom.

For this control scheme to work, both the $f_1$ and $f_2$ sidebands are resonant in the PRC, the $f_2$, but not the $f_1$, sidebands are resonant in the SRC, and neither the $f_1$ or $f_2$ sidebands are resonant in the arms. Here we are only concerned with how the macroscopic lengths of the recycling cavities are chosen to satisfy these conditions.

The reflectivity of a Fabry-Perot cavity of length $L$ with input mirror reflectivity $r_i$ and end mirror reflectivity $r_e$ is
\begin{equation}
r(\phi) = \frac{-r_i + r_e \e^{-2\i\phi}}{1 - r_i r_e \e^{-2\i\phi}}
\label{fp-reflectivity}
\end{equation}
where $\phi = \omega L/c$ is the phase a field of frequency $\omega$ accrues going one-way along the cavity. Eq.~\eqref{fp-reflectivity} also holds when the mirrors are compound cavities with complex reflectivities. A cavity is said to be resonant if the round-trip phase is zero and anti-resonant if the round-trip phase is $\pi$. Taking the possibly complex arm reflectivities into account, these conditions are
\begin{subequations}
\begin{align}
\hspace{0.2\textwidth} &\text{resonant}: & \arg{(r_i r_e \e^{-2\i\phi})} &= 2\pi n \hspace{0.2\textwidth}\\
&\text{anti-resonant}: &\arg{(r_i r_e \e^{-2\i\phi})} &= (2k + 1)\pi.
\end{align}
\end{subequations}
Eq.~\eqref{fp-reflectivity} is the fundamental relation that sets the macroscopic cavities lengths.

\section{Arm Cavities}

The arm cavities are chosen to be resonant for the carrier $\omega_0$ and (nearly) anti-resonant for the sidebands $\omega_0 \pm \Omega_i$. The end mirrors are highly reflective $r_e\approx 1$ and so
\begin{equation}
r_\t{arm}(\omega_0) = \frac{-r_\t{itm} + 1}{1 - r_\t{itm}} = 1.
\end{equation}
If the sidebands were exactly anti-resonant
\begin{equation}
r_\t{arm}(\omega_0 + \Omega_i) = \frac{-r_\t{itm} - 1}{1 + r_\t{itm}} = -1.
\end{equation}
In practice the sidebands are chosen to not be exactly anti-resonant in order to avoid higher harmonics from resonating in the arms. The sidebands thus have complex reflectivities
\begin{equation}
r_\t{arm}(\omega_0 + \Omega_i) = \abs{r_\t{arm}(\Omega_i)}\e^{\i \theta_i}
\label{arm-reflectivity}
\end{equation}
where $\abs{\pi - \theta_i} \ll 1$. For the 40~m arm cavities, $\theta_1 = 180.5^\circ$ and $\theta_2  = 182.5^\circ$.

\section{Power Recycling Cavity}

The power recycling cavity length is chosen so that the carrier and both sidebands are resonant in the PRC when the arms are resonant for the carrier. For the carrier
\begin{equation}
\arg \Big[ r_\t{prm} r_\t{arm}(\omega_0) \e^{-2\i\omega_0 L_\t{prc}/c} \Big] = 2\pi n
\end{equation}
and so $L_\t{prc}$ is microscopically adjusted such that $\omega_0 L_\t{prc} / c = n\pi$. For the $f_1$ sideband
\begin{equation}
\arg \Big[r_\t{prm} \abs{r_\t{arm}(\Omega_1)}\e^{\i\theta_1} \e^{-2\i\omega_0 L_\t{prc}/c}
\e^{-2\i\Omega_1 L_\t{prc}/c} \Big]
= \theta_1 + 0 - \frac{2\Omega_1 L_\t{prc}}{c}
= 2\pi n
\end{equation}
and so the power recycling length must satisfy
\begin{equation}
L_\t{prc} = \left(k + \frac{\theta_1}{2\pi} \right) \frac{c}{2f_1}.
\label{prc-condition}
\end{equation}
If \eqref{prc-condition} is satisfied for $f_1$ then it is automatically satisfied for $f_2 = 5f_1$ if $\theta_2 - \pi = 5(\theta_1 - \pi)$. For the 40~m, we choose $k=0$ giving $L_\t{prc} = 6.753\,\t{m}$.


\section{Signal Recycling Cavity}

The signal recycling cavity length is chosen so that the $f_2$ sideband is resonant and the $f_1$ sideband is non-resonant in the SRC when the arms are resonant for the carrier. Since the phase the carrier accrues in the SRC differs between signal recycling and resonant sideband extraction, the two cases have different requirements for the SRC length.

\paragraph{Signal Recycling} With signal recycling the carrier does not acquire any phase in the SRC. This is the same as with the PRC. Since the $f_2$ sideband is also resonant in the PRC, the same condition \eqref{prc-condition} is necessary for $L_\t{src}$ and $f_2$. Since $f_1$ has to be non-resonant in the SRC, however, the conditions that must be simultaneously satisfied are
\begin{subequations}
\label{src-conditions}
\begin{align}
L_\t{src} &= \left( n + \frac{\theta_2}{2\pi}\right) \frac{c}{2f_2}\\
L_\t{src} &\neq \left( m + \frac{\theta_1}{2\pi}\right) \frac{c}{2f_1}.
\end{align}
\end{subequations}
For the 40~m the first three lengths satisfying \eqref{src-conditions} are $1.336, 4.044,$ and $9.463~\t{m}$. For the ponderomotive squeezing experiment described in Chap.~\ref{chap:40m}, we choose $n=1$ giving $L_\t{src} = 4.044~\t{m}$.

\paragraph{Resonant Sideband Extraction} With RSE, the carrier has a one-way phase shift of $\pi/2$. So for the $f_2$ sideband to be resonant in the SRC,
\begin{equation}
\arg \Big[r_\t{srm} \abs{r_\t{arm}(\Omega_2)}\e^{\i\theta_2} \e^{-2\i\omega_0 L_\t{src}/c}
\e^{-2\i\Omega_2 L_\t{src}/c} \Big]
= \theta_2 + \pi - \frac{2\Omega_2 L_\t{src}}{c}
= 2\pi n.
\end{equation}
The conditions that must be simultaneously satisfied are thus
\begin{subequations}
\begin{align}
L_\t{src} &= \left( n + \frac{1}{2} + \frac{\theta_2}{2\pi}\right) \frac{c}{2f_2}\\
L_\t{src} &\neq \left( m + \frac{1}{2}  + \frac{\theta_1}{2\pi}\right) \frac{c}{2f_1}.
\end{align}
\end{subequations}

\paragraph{General Detuning} For a general detuning, leading to an optical spring, the carrier picks up a one-way phase of $\phi$. Thus, for $f_2$ to be resonant,
\begin{equation}
\arg \Big[r_\t{srm} \abs{r_\t{arm}(\Omega_2)}\e^{\i\theta_2} \e^{2\phi}
\e^{-2\i\Omega_2 L_\t{src}/c} \Big]
= \theta_2 + 2\phi - \frac{2\Omega_2 L_\t{src}}{c}
= 2\pi n.
\end{equation}
The conditions that must be simultaneously satisfied are thus
\begin{subequations}
\begin{align}
L_\t{src} &= \left( n  + \frac{2\phi + \theta_2}{\pi} \right) \frac{c}{2f_2}\\
L_\t{src} &\neq \left( m  + \frac{2\phi + \theta_1}{\pi} \right) \frac{c}{2f_1}\\
\end{align}
\end{subequations}

\section{Schnupp Asymmetry}

\begin{figure}
\centering
\includegraphics[scale=1]{Chapters/RecyclingCavities/Figures/DRMI.pdf}
\caption{Recycling cavity lengths}
\label{fig:cavity-lengths}
\end{figure}

Once the lengths of the recycling cavities are set, the Schnupp asymmetry is chosen so that the $f_2$ sideband is critically coupled into the SRC. To find the couplings we need the transmission from the PRC to the SRC in a dual recycled Michelson interferometer with X and Y mirror reflectivities given by \eqref{arm-reflectivity}. With the lengths defined as
\begin{equation}
L_\t{prc} = L_p + \frac{l_y + l_x}{2},\qquad
L_\t{src} = L_s + \frac{l_y + l_x}{2}, \qquad
l_\t{sch} = l_y - l_x,
\end{equation}
see Fig.~\ref{fig:cavity-lengths}, we define the following phases:
\begin{subequations}
\begin{align}
\phi_x &= \frac{\omega l_x}{c}, \qquad
\phi_y = \frac{\omega l_y}{c}, \qquad
\phi_\pm = \frac{\phi_y \pm \phi_x}{2} = \frac{\omega l_\t{sch}}{2c}, \\
\phi_p &= \frac{\omega L_p}{c}, \qquad
\phi_\t{prc} = \frac{\omega L_\t{prc}}{c} = \phi_p + \phi_+, \\
\phi_s &= \frac{\omega L_s}{c}, \qquad
\phi_\t{src} = \frac{\omega L_\t{src}}{c} = \phi_s + \phi_+.
\end{align}
\end{subequations}

\begin{figure}
\centering
\includegraphics[scale=0.37]{Chapters/RecyclingCavities/Figures/sideband_transmission.pdf}
\caption[RF sideband transmission to the asymmetric port]{RF sideband transmission to the asymmetric port for the 40~m interferometer configured for signal recycling. The maximum transmission for $f_2$ is $0.997$ and occurs for $l_\t{sch} = 23.2\,\t{mm}$.}
\label{fig:transmission}
\end{figure}


The transmission and reflection of a simple Michelson formed by the beam splitter and end mirrors with reflectivities given by \eqref{arm-reflectivity} are
\begin{align}
t_\t{mich} &= \frac{r_\t{arm}}{2} \e^{-2\i\phi_+}\left( \e^{2\i\phi_-} - \e^{-2\i\phi_-}\right)
= \i r_\t{arm} \e^{-2\i\phi_+} \sin 2\phi_- \\
r_\t{mich} &= \frac{r_\t{arm}}{2} \e^{-2\i\phi_+}\left( \e^{2\i\phi_-} + \e^{-2\i\phi_-}\right)
= r_\t{arm} \e^{-2\i\phi_+} \cos 2\phi_-.
\end{align}
Using this, the transmission from the PRC to SRC is
\begin{equation}
t_{\t{prc}\to\t{src}} = t_\t{prm} t_\t{srm}\frac{\i r_\t{arm} \e^{-\i(\phi_+ + \phi_\t{src})} \sin 2\phi_-}
{1 - r_\t{arm} \left( r_\t{prm} \e^{-2\i\phi_\t{prc}} + r_\t{srm} \e^{-2\i\phi_\t{src}} \right) \cos 2\phi_-
+ r_\t{arm}^2 r_\t{prm} r_\t{srm} \e^{-2\i (\phi_\t{prc} + \phi_\t{src})}}.
\label{PRC-transmission}
\end{equation}
Eq.~\eqref{PRC-transmission} can be simplified by noting that all fields we are considering are resonant in the PRC and so $\theta - 2\phi_\t{prc} = 0$:
\begin{equation}
t_{\t{prc}\to\t{src}} = t_\t{prm} t_\t{srm} \frac{\i r_\t{arm} \e^{-\i(\phi_+ + \phi_\t{src})} \sin 2\phi_-}
{1 - \abs{r_\t{arm}} \left[ r_\t{prm} + r_\t{srm} \e^{\i(\theta -2\phi_\t{src})} \right] \cos 2\phi_-
+  \abs{r_\t{arm}}^2 r_\t{prm} r_\t{srm} \e^{\i(\theta - 2\phi_\t{src})}}.
\label{PRC-transmission-simplified}
\end{equation}
Eq.~\eqref{PRC-transmission-simplified} must be used for general fields resonant in the PRC. However, for $f_2$ which also must be resonant in the SRC, $\theta_2 - 2\phi_\t{src} = 0$ and the transmission is
\begin{equation}
t_{\t{prc}\to\t{src}}(f_2) = t_\t{prm} t_\t{srm}\frac{\i r_\t{arm} \e^{-\i(\phi_+ + \phi_\t{src})} \sin 2\phi_-}
{1 - \abs{r_\t{arm}} \left( r_\t{prm} + r_\t{srm} \right) \cos 2\phi_-
+  \abs{r_\t{arm}}^2 r_\t{prm} r_\t{srm}}.
\label{PRC-transmission-f2}
\end{equation}
Fig.~\ref{fig:transmission} shows the power transmissivity of the $f_1$ and $f_2$ sidebands to the asymmetric port as a function of Schnupp asymmetry $l_\t{sch}$ for the 40~m. The maximum power transmission for $f_2$ is $0.997$ and occurs at $l_\t{sch}=23.3\,\t{mm}$.
