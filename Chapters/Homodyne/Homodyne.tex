\newcommand{\F}{\mathcal{F}}

\chapter{Homodyne Detection}
\label{chap:homodyne}

This appendix describes how signals are measured with balanced homodyne detection (BHD). Sec.~\ref{sec:basic-bhd} describes the basic BHD setup where a local oscillator (LO) at the same frequency as the signal is used to optically demodulate the signal down from optical frequencies. As mentioned in Appendix.~\ref{chap:recycling-lengths}, the laser is phase modulated with two sets of RF sidebands before entering the interferometer. These sidebands need to be removed using an output mode cleaner (OMC). Sec.~\ref{sec:two-pol-omc} describes an idea due to Koji Arai~\cite{PolarizationBHD} which cleans both the LO and the signal with a single OMC by making the signal p-polarized and the LO s-polarized. Secs.~\ref{sec:before}--\ref{sec:both} analyze the displacement noise of this setup.

\section{Basic Balanced Homodyne Detection}
\label{sec:basic-bhd}

\begin{figure}
\centering
\includegraphics[scale=1]{Chapters/Homodyne/Figures/SimpleBHD.pdf}
\caption{Basic balanced homodyne detector}
\label{fig:basic-bhd}
\end{figure}

The basic balanced homodyne detector is shown in Fig.~\ref{fig:basic-bhd}. A strong local oscillator $E_\t{LO}\e^{\i\zeta} = \sqrt{P_\t{LO}}\e^{\i(\zeta + \omega t)}$ is mixed with the signal from the interferometer $E_\t{IFO} = \sqrt{P_\t{IFO}}\e^{\i\omega t}$ on a beam splitter where $P_\t{LO}$ and $P_\t{IFO}$ are the LO and interferomter power and $\zeta$ is the angle between the interferometer and LO quadratures. Assuming $P_\t{LO}\gg P_\t{IFO}$ the power on the two photodiodes in Fig.~\ref{fig:basic-bhd} is
\begin{subequations}
\begin{align}
P_A &= \frac{1}{2}\left[P_\t{LO} - 2\sqrt{P_\t{LO}P_\t{IFO}}(\e^{\i\zeta} + \e^{-\i\zeta})\right]\\
P_B &= \frac{1}{2}\left[P_\t{LO} + 2\sqrt{P_\t{LO}P_\t{IFO}}(\e^{\i\zeta} + \e^{-\i\zeta})\right].
\end{align}
\end{subequations}
Subtracting the two photocurrents thus gives a signal proportional to the interferometer signal
\begin{equation}
P = \frac{P_B - P_A}{2} = \sqrt{P_\t{LO}P_\t{IFO}}\cos\zeta.
\label{basic-bhd}
\end{equation}
Ref.~\cite{Steinlechner2015} analyzes noise from nonidealities in a BHD.

\section{Two polarization OMC for BHD}
\label{sec:two-pol-omc}

When using a BHD to measure the signal from an interferometer, as in the experiment described in Chap.~\ref{chap:40m} and Fig.~\ref{fig:DRFPMI}, the RF sidebands need to be cleaned off of both the signal and LO. A scheme for using a single OMC to filter both the LO and signal is shown in Fig.~\ref{fig:OMC}. The signal from the interferometer is p-polarized and the LO is s-polarized. Since the signal and LO are in orthogonal quadratures they do not mix while they are simultaneously resonating in the OMC. A polarized beam splitter is placed before the two photodiodes. Before the signal and LO reach the PBS they are rotated by $\pi/4$. Then, after the beam splitter the s-polarized field has signal and LO in phase and the p-polarized signal has the signal and LO out of phase. The signals from both photodiodes can then be subtracted as in regular BHD \eqref{basic-bhd}.

As shown in Fig.~\ref{fig:OMC}, a half-wave plate can be used to rotate the polarizations by $\pi/4$ either before or after the OMC. These cases are analyzed in Secs.~\ref{sec:before} and \ref{sec:after}, respectively. In general the polarizations just need to have a net rotation of $\pi/4$ before the second PBS. It is thus possible to have a half-wave plate both before and after the OMC. This case is analyzed in Sec.~\ref{sec:both}.

\begin{figure}
\begin{centering}
\includegraphics[width=\textwidth]{Chapters/Homodyne/Figures/OMC.pdf}
\caption[Polarization OMC for BHD]{Polarization OMC for BHD}
\label{fig:OMC}
\end{centering}
\end{figure}

With the standard BHD scheme a single beam splitter is used. No extra quantum noise is added by this setup since there are no open ports for vacuum fluctations to enter through. On the other hand, the setup shown in Fig.~\ref{fig:OMC} has two PBS with one open port each. Vacuum noise enters each of these open ports but does not add any extra quantum noise. For both PBS the vacuum that enters the system is polarized orthogonal to the signal. For the PBS before the PDs, PDB measures the p-polarized signal and PDA measures the s-polarized signals. But the s-polarized vacuum entering the PBS's open port is reflected to PDB and the p-polarized vacuum is transmitted to PDA. Similarly, no extra noise is added from the first PBS.

In the following, the field from the interferometer $E_\t{IFO}$ is p-polarized and the LO field $E_\t{LO}$ is s-polarized. The p-component is the field incident on PDB and the s-component is the field incident on PDA.

The OMC mirror coatings are birefringent and do not have the same reflectivity for s- and p-polarizations. The two polarizations have different cavity finesse and free spectral ranges and have different transfer functions through the OMC. The OMC is critically coupled and so the transfer function is approximately
\begin{equation}
G(f) = \frac{1}{1 + \i f/f_c} = \frac{\e^{\i\phi}}{1 + (f/f_c)^2} = \abs{G}\e^{\i\phi}
\end{equation}
where $f_c = f_\t{FSR}/2\F$ is the cavity pole. We are interested in frequencies $f\ll f_c$. In this case the magnitude $\abs{G}\approx 1$ and the phase $\phi\approx 0$. Since s and p have different transfer functions, we keep $\abs{G}$ and $\phi$ general below to analyze the effects of OMC length fluctuations. However, once these effects have been calculated we can set $\abs{G}=1$ and $\phi=0$ for both s and p and obtain the standard BHD signal \eqref{basic-bhd}.

The circulating power for the s- and p-polarizations in the OMC is shown schematically in Fig.~\ref{fig:circ_power}. The s-polarization has a higher finesse and a larger FSR than the p-polarization and so the circulating s-polarization is resonant at a higher frequency than the p-polarization. The cavity can be locked halfway between the two resonances.


\begin{figure}
\begin{centering}
\includegraphics[scale=0.4]{Chapters/Homodyne/Figures/OMC_circ_power.pdf}
\caption[OMC circulating power]{OMC circulating power. The cavity finesse plotted is a factor of 10 less than the aLIGO OMCs and the frequency shift is exaggerated. For the aLIGO OMCs, $\delta f_\t{FSR}/f_\t{FSR}\approx 10^{-5}$.}
\label{fig:circ_power}
\end{centering}
\end{figure}


\section{Noise with the half-wave plate before the OMC}
\label{sec:before}

With the HWP before the OMC, the polarizations are rotated before entering the OMC. The field incident on the second PBS is thus
\begin{equation}
E = \frac{1}{\sqrt{2}}G_p\left(E_\t{IFO} - E_\t{LO}\e^{\i\zeta}\right)\,\rf{p}
 + \frac{1}{\sqrt{2}}G_s\left(E_\t{IFO} + E_\t{LO}\e^{\i\zeta}\right)\,\rf{s}.
\end{equation}
and the homodyne signal is
\begin{equation}
P = \frac{1}{4}(P_\t{IFO} + P_\t{LO})\left(\abs{G_s}^2 - \abs{G_p}^2\right)
+ \frac{1}{2}\sqrt{P_\t{IFO} P_\t{LO}}\, \left(\abs{G_s}^2 + \abs{G_p}^2\right)\cos\zeta.
\label{P-homo-before}
\end{equation}

Since the cavity is locked off resonance for both polarizations, OMC length fluctuations lead to first order fluctuations in the circulating power. The slope of the magnitude of the transfer function is
\begin{equation}
\pderiv{\abs{G}^2}{f} = - \frac{2f/f_c^2}{[1 + (f/f_c)^2]^2}.
\end{equation}
Since the cavity is locked halfway between the two resonances at $\pm\delta f_\t{FSR}/2$, for $f\ll f_c$,
\begin{equation}
\pderiv{\abs{G_p}^2}{x} = \frac{\delta f_\t{FSR}}{f_c^2} \frac{f_\t{FSR}}{\lambda}
= - \pderiv{\abs{G_s}^2}{x}.
\end{equation}
If the cavity gets longer the p-polarized circulating power increases and the s-polarized power decreases, and vice versa.

The power fluctuations in the homodyne power \eqref{P-homo-before} due to OMC length fluctuations are thus
\begin{equation}
\pderiv{P}{x} = \frac{1}{4}(P_\t{IFO} + P_\t{LO})(-2) \frac{f_\t{FSR}}{\lambda}
\frac{\delta f_\t{FSR}}{f_c^2}.
\end{equation}
The power fluctuations in the beet between the LO and signal cancel while the fluctuations in the DC power add. The OMC displacement noise is thus
\begin{equation}
S_P^{1/2} = \frac{P_\t{IFO} + P_\t{LO}}{2} \frac{f_\t{FSR}}{\lambda}
\frac{\delta f_\t{FSR}}{f_c^2} S_x^{1/2}.
\end{equation}

\section{Noise with the half-wave plate after the OMC}
\label{sec:after}

With the HWP after the OMC the field incident on the second PBS is
\begin{equation}
E = \frac{1}{\sqrt{2}}\left(G_p E_\t{IFO} - G_s E_\t{LO}\e^{\i\zeta}\right)\rf{p}
+ \frac{1}{\sqrt{2}}\left(G_p E_\t{IFO} + G_s E_\t{LO}\e^{\i\zeta}\right)\rf{s}
\end{equation}
and the homodyne signal is thus
\begin{equation}
P = \sqrt{P_\t{IFO} P_\t{LO}}\, \abs{G_p}\abs{G_s}\cos(\zeta + \Delta\phi).
\end{equation}
where $\Delta\phi=\phi_s - \phi_p$. As with the case above, power fluctuations in s and p due to length fluctuations cancel.

However, since s and p have different cavity finesse, the phase change due to cavity length fluctuations is different:
\begin{equation}
\pderiv{\phi}{x} = \frac{2\F}{\pi} \frac{2\pi}{\lambda}
\qquad \Rightarrow \qquad \pderiv{\Delta\phi}{x} = \frac{4\delta\F}{\lambda}
\end{equation}
where $\delta\F = \F_s - \F_p$. The noise is thus
\begin{equation}
S_P^{1/2} = \sqrt{P_\t{IFO} P_\t{LO}}\, \frac{4\delta\F}{\lambda}
\abs{\sin\zeta} S_x^{1/2}.
\label{disp-noise-after}
\end{equation}
This noise could be significant if $P_\t{IFO}$ as a large contrast defect component. This noise is largest when reading out the phase quadrature since the LO and CD are orthogonal. However, there is no displacement noise when reading out the amplitude quadrature. In this case the LO and CD are aligned and so there is no first order change in the rates of rotation for the s- and p-polarizations.

\section{Noise with half-wave plates on both sides of the OMC}
\label{sec:both}

Since it is only necessary that there is a net $\pi/4$ polarization rotation before the second PBS, it is possible to us a HWP before the OMC that rotates the polarizations by $\theta$ and a second HWP after the OMC which rotates the polarizations by $\varphi = \pi/4 - \theta$. In this case, the fields after the PBS are
\begin{equation}
\bbmat E_A \\
E_B \ebmat
=
\frac{1}{\sqrt{2}} \bbmat
\cos\theta + \sin\theta & \sin\theta - \cos\theta \\
\cos\theta - \sin\theta & \cos\theta + \sin\theta
\ebmat \bbmat
G_p\cos\theta & -G_p\sin\theta\\
G_s\sin\theta & G_s\cos\theta
\ebmat \bbmat
E_\t{IFO}\\
E_\t{LO}\e^{\i\zeta}
\ebmat
\end{equation}
where the top component is p and the bottom component is s. In this case it can be shown that $P = P_1 + P_2$ where
\begin{subequations}
\begin{multline}
P_1 = \frac{1}{8}[(\sin4\theta - 2\sin2\theta)
(\abs{G_p}^2 P_\t{LO} - \abs{G_s}^2 P_\t{IFO})\\
+ (\sin4\theta + 2\sin2\theta)
(\abs{G_s}^2 P_\t{LO} - \abs{G_p}^2 P_\t{IFO})]
- \frac{1}{4}\sin4\theta\,\abs{G_p}\abs{G_s}(P_\t{LO} - P_\t{IFO})\cos\Delta\phi
\end{multline}
and
\begin{multline}
P_2 = \frac{1}{2}\sqrt{P_\t{LO}P_\t{IFO}} \{\sin^22\theta\, (\abs{G_s}^2 + \abs{G_p}^2)\cos\zeta \\
+ 2\abs{G_s}\abs{G_p}\cos2\theta \left[ \cos^2\theta \cos(\zeta + \Delta\phi)
-\sin^2\theta\cos(\zeta - \Delta\phi)\right]\}.
\end{multline}
\end{subequations}
Note that when $\abs{G_s}=\abs{G_p} = 1$ and $\Delta\phi=0$, $P_1=0$ and $P_2=\sqrt{P_\t{LO}P_\t{IFO}}\cos\zeta$ regardless of $\theta$, as it should.

\begin{figure}
\begin{centering}
\includegraphics[scale=0.4]{Chapters/Homodyne/Figures/OMCtf.pdf}
\caption[OMC sensitivity to displacement noise]{OMC sensitivity to displacement noise when the phase quadrature ($\zeta=\pi/2$) is readout. The parameters for the aLIGO OMCs are used with $P_\t{LO}=10\,\t{mW}$ and $P_\t{CD} = 1\,\t{mW}$. The line marked $\delta f_\t{FSR}$ is the sensitivity due to the FSR difference which would be obtained if a single HWP is placed before the OMC ($\theta=\pi/4$). The line marked $\delta\F$ is the sensitivity due to the finesse difference which would be obtained if a single HWP is placed after the OMC ($\theta=0$).}
\label{fig:OMCtf}
\end{centering}
\end{figure}

The length fluctuations of these terms are
\begin{subequations}
\begin{align}
\pderiv{P_1}{x} &= - \frac{1}{2} \frac{f_\t{FSR}}{\lambda}\frac{\delta f_\t{FSR}}{f_c^2}(P_\t{LO} + P_\t{IFO})\sin2\theta\\
\pderiv{P_2}{x} &= - \sqrt{P_\t{IFO}P_\t{LO}} \frac{4\delta\F}{\lambda}
\cos 2\theta \sin\zeta
\end{align}
\end{subequations}
and so the total displacement noise is
\begin{equation}
S_P^{1/2} = \abs{\frac{1}{2}(P_\t{LO} + P_\t{IFO})\frac{f_\t{FSR}}{\lambda}
\frac{\delta f_\t{FSR}}{f_c^2} \sin2\theta
+ \sqrt{P_\t{LO} P_\t{IFO}} \frac{4\delta\F}{\lambda}\sin\zeta \cos2\theta} S_x^{1/2}.
\end{equation}
It is thus possible to cancel the displacement noise by choosing the polarization angle appropriately. In the case that the interferometer beam is dominated by contrast defect $P_\t{CD}$ and $P_\t{LO}\gg P_\t{CD}$, the optimal angle is
\begin{equation}
\theta_\t{opt} = -\frac{1}{2}\arctan \left( \sqrt{\frac{P_\t{CD}}{P_\t{LO}}}
\frac{8\delta\F}{f_\t{FSR}} \frac{f_c^2}{\delta f_\t{FSR}}\sin\zeta\right).
\end{equation}
Note that the angle is $\pi/2$ periodic.


\begin{figure}
\begin{centering}
\includegraphics[scale=0.4]{Chapters/Homodyne/Figures/OMC_opt_angle.pdf}
\caption[Optimal polarization angle]{Optimal polarization angle for an aLIGO OMC with $P_\t{LO}=10\,\t{mW}$ and $P_\t{CD} = 1\,\t{mW}$.}
\label{fig:pol-angle}
\end{centering}
\end{figure}


For the aLIGO OMCs, $\delta\F\approx 80$, $\delta f_\t{FSR}\approx 30\,\t{kHz}$, $f_\t{FSR}\approx 268\, \t{MHz}$, and $f_c\approx 340\,\t{kHz}$. The coatings can presumably be designed to minimize the birefringence, but for concreteness the sensitivity of the OMC to displacement noise $S_P^{1/2}/S_x^{1/2}$ when the phase quadrature $\zeta=\pi/2$ is readout is shown in Fig.~\ref{fig:OMCtf} for aLIGO parameters. The optimal rotation angle as a function of homodyne angle is plotted in Fig.~\ref{fig:pol-angle}.
