from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.lines as mlines
from scipy.interpolate import interp1d
import scipy.constants as scc
import h5py
import sys
import subprocess
sys.path.append('..')
import BnC as bnc

plt.ion()

mpl.rc('figure', figsize=(12, 9))

savefigs = True

if savefigs:
    mpl.rcParams.update({'text.usetex': True,
                         'font.family': 'serif',
                         # 'font.serif': 'Georgia',
                         # 'mathtext.fontset': 'cm',
                         'lines.linewidth': 2.5,
                         'font.size': 22,
                         'xtick.labelsize': 'large',
                         'ytick.labelsize': 'large',
                         'legend.fancybox': True,
                         'legend.fontsize': 18,
                         'legend.framealpha': 0.7,
                         'legend.handletextpad': 0.5,
                         'legend.labelspacing': 0.2,
                         'legend.loc': 'best',
                         'savefig.dpi': 80,
                         'pdf.compression': 9})
else:
    mpl.rcParams.update({'text.usetex': False,
                         'font.family': 'serif',
                         'font.serif': 'Georgia',
                         'mathtext.fontset': 'cm',
                         'lines.linewidth': 2,
                         'font.size': 16,
                         'legend.loc': 'best',
                         'savefig.dpi': 80,
                         'pdf.compression': 9})


def coilDriverNoise(ff, Rfast, Rslow, voltNoise_fast, Ncoils, i2f=0.016):
    """Coil driver noise in [m/rtHz]

    Inputs:
        ff: frequency vector [Hz]
        Rfast: series resistance of the fast path [Ohms]
        Rslow: series resistance of the slow path [Ohms]
        voltageNoise_fast: voltage noise of the fast pasth [V/rtHz]
        Ncoils: number of coils
        i2f: conversion between current to the coil driver and applied force [N/A]
    """
    voltNoise_slow = np.sqrt(4*Rslow*scc.k*298)
    currNoise_slow = voltNoise_slow / Rslow
    currNoise_fast = voltNoise_fast / Rfast
    currNoise = np.sqrt(Ncoils) * np.sqrt(currNoise_slow**2 + currNoise_fast**2)
    dispNoise = i2f*currNoise / ((2*np.pi*ff)**2 * M)
    return dispNoise


def computeNoise(Pin, prg, Ts, phi, zeta):
    """Compute quantum and classical noise

    Inputs:
      Pin: power on the back of PRM [W]
      prg: power recycling gain
      Ts: SRM transmissivity
      phi: SRC detuning [deg]
      zeta: homodyne angle [deg]
    """
    Pbs = prg*Pin
    _, Sv_quant, D, _, _, _ = bnc.ioLossy(
        ff, phi, zeta, Ts, Pbs, Titm, M, Larm, Lrt, epsSR, epsPD)
    tf = D[0, :]*np.sin(zeta*np.pi/180) + D[1, :]*np.cos(zeta*np.pi/180)
    tf = tf/Larm
    Sv_class = np.abs(tf) * h_class
    Sv_tot = np.sqrt(Sv_class**2 + Sv_quant**2)
    Sv_quant = 20*np.log10(Sv_quant)
    Sv_class = 20*np.log10(Sv_class)
    Sv_tot = 20*np.log10(Sv_tot)
    return Sv_quant, Sv_tot


def printStats(Stot):
    Smin = np.min(Stot)
    fmin = ff[np.argmin(Stot)]
    return Smin, fmin


data = h5py.File('noises.hdf5', 'r')

fnum = 500  # number of frequency points to evaluate
fmin = 20  # minimum frequency [Hz]
fmax = 6e3  # maximum frequency [Hz]
ff = np.logspace(np.log10(fmin), np.log10(fmax), fnum)
Larm = 37.795
M = 0.2642
Titm = 0.014
# prg = 80  # power recycling gain
# Pprm = 30  # powr on the back of PRM
# Pbs = prg*Pprm  # power in the PRC
Lrt = 20e-6  # round trip losses in the arms
epsPD = 0.05  # PD losses
epsSR = 150e-6  # loss in the SRC

Rfast = 10e3
Rslow = 25e3

# Displacement noises, all in m/rtHz
h_ctn = interp1d(
    data['mirrorThermalNoise_ff'], data['mirrorThermalNoise'],
    bounds_error=False)(ff)
h_sus = interp1d(
    data['suspThermNoise_ff'], data['suspThermNoise'], bounds_error=False)(ff)
h_sei = interp1d(data['seis_ff'], data['seis'], bounds_error=False,
                 fill_value='extrapolate')(ff)
h_newt = interp1d(
    data['newtonianNoise_ff'], data['newtonianNoise'], bounds_error=False)(ff)
h_rg = interp1d(data['resGasNoise_ff'], data['resGasNoise'],
                bounds_error=False)(ff)
voltNoise_fast = interp1d(data['CD_fast_4k_ff'], data['CD_fast_4k'])(ff)

h_coil = np.sqrt(2)*coilDriverNoise(ff, Rfast, Rslow, voltNoise_fast, 4)
h_class = np.sqrt(h_ctn**2 + h_sei**2 + h_sus**2 + h_coil**2
                  + h_rg**2 + h_newt**2)

data.close()

p1 = [1, 40, 0.099, 0.002, 89.7]
p2 = [1, 80, 0.099, 0.004, 89.7]
p3 = [10, 80, 0.099, 0.015, 89.7]
# p4 = [10, 80, 0.245, 0.035, 89.7]
p5 = [30, 80, 0.245, 0.275, 88]

Sq1, St1 = computeNoise(*p1)
Sq2, St2 = computeNoise(*p2)
Sq3, St3 = computeNoise(*p3)
# Sq4, St4 = computeNoise(*p4)
Sq5, St5 = computeNoise(*p5)

alpha = 0.35

fig = plt.figure()
ax = fig.gca()
ax.semilogx(ff, St1, label='Case A')
ax.semilogx(ff, Sq1, 'C0-.', alpha=alpha)
ax.semilogx(ff, St2, label='Case B')
ax.semilogx(ff, Sq2, 'C1-.', alpha=alpha)
ax.semilogx(ff, St3, label='Case C')
ax.semilogx(ff, Sq3, 'C2-.', alpha=alpha)
# ax.semilogx(ff, St4, label='4')
ax.semilogx(ff, St5, label='Case D')
ax.semilogx(ff, Sq5, 'C3-.', alpha=alpha)

ax.grid(True, which='both', alpha=alpha)
ax.grid(which='minor', alpha=0.2)
ax.set_xlim(ff[0], ff[-1])
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('Noise below vacuum [dBvac]')
ax.set_title('Classical and Quantum Noise')
ax.set_ylim(-12, 10)

ax.legend()
hOld, lOld = ax.get_legend_handles_labels()
l1 = mlines.Line2D([], [], ls='-', c='k')
l2 = mlines.Line2D([], [], ls='-.', c='k', alpha=alpha)
handles = [l1, l2]
labels = ['Total noise', 'Quantum noise']
handles.extend(hOld)
labels.extend(lOld)
ax.legend(handles, labels, loc='lower right')


if savefigs:
    sname = 'Figures/future_squeezing.pdf'
    fig.savefig(sname)
    cmd = ['pdfcrop', sname, sname]
    subprocess.call(cmd)
