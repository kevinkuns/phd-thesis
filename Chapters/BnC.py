from __future__ import division
import numpy as np
import scipy.constants as scc
import matplotlib.pyplot as plt


# phi = 0
# zeta = 90
# Tsrm = 0.2
# Ti = 0.01
# Pbs = 125
# m = 40
# Larm = 4e3

# ff = np.logspace(0, 4, 500)

def io(ff, phi, zeta, Tsrm, Pbs, Ti=0.01, m=40, Larm=4e3, phase=True):
    """Compute lossless BnC input/output relations

    Inputs:
      ff: frequency points [Hz]
      phi: SRC detuning [deg]
      zeta: homodyne angle [deg]
      Tsrm: SRM transmissivity
      Pbs: power on the BS [W]
      Ti: ITM transmissivity (Default: 0.01)
      m: test mass mass [kg] (Default: 40 kg)
      Larm: arm length [m] (Default: 4 km)
      phase: if True multiply the appropriate IO matrices by np.exp(2j*beta)
        (Default: True)

    Returns:
      C: the IO matrix from in vacuum to out vacuum
      Sb_vac: the (non-signal referred) quantum *amplitude* spectral density
      D: the IO matrix from mirror strain to out vacuum

    Note that the IO vacuum matrix has shape (2, 2, len(ff))
    (or (2, 2) if ff is a single frequency point),
    and the IO signal matrix has shape (2, len(ff))
    (or (2,) if ff is a single frequency point).
    """
    # Compute some basic quantities
    phi = phi * np.pi/180
    zeta = zeta * np.pi/180
    ww = -2*np.pi*ff
    w0 = 2*np.pi*scc.c / 1064e-9
    gamma = scc.c*Ti / (4*Larm)
    beta = np.arctan(ww/gamma)
    ts = np.sqrt(Tsrm)
    rs = np.sqrt(1 - Tsrm)

    # IO quantities
    Isql = m * Larm**2 * gamma**4 / (4*w0)
    hsql = np.sqrt(8*scc.hbar / (m * ww**2 * Larm**2))
    K = 2*Pbs/Isql * gamma**4 / (ww**2 * (gamma**2 + ww**2))

    # Normalization
    M = (1 + rs**2 * np.exp(4j*beta)
         - 2*rs*np.exp(2j*beta) * (np.cos(2*phi) + K/2 * np.sin(2*phi)))

    # IO matrix
    C11 = ((1 + rs**2) * (np.cos(2*phi) + K/2 * np.sin(2*phi))
           - 2*rs*np.cos(2*beta))
    C12 = -ts**2 * (np.sin(2*phi) + K*np.sin(phi)**2)
    C21 = ts**2 * (np.sin(2*phi) - K*np.cos(phi)**2)
    C = np.array([[C11, C12], [C21, C11]]) / M
    if phase:
        C = np.exp(2j*beta) * C

    # transfer function
    D1 = -(1 + rs*np.exp(2j*beta)) * np.sin(phi)
    D2 = -(-1 + rs*np.exp(2j*beta)) * np.cos(phi)
    D = np.sqrt(2*K)*ts*np.exp(1j*beta)/(hsql*M) * np.array([D1, D2])

    # ASD
    Sb_vac = np.sqrt(np.abs(C[0, 0]*np.sin(zeta) + C[1, 0]*np.cos(zeta))**2
                     + np.abs(C[0, 1]*np.sin(zeta) + C[1, 1]*np.cos(zeta))**2)

    #
    num = ((4*rs**2 * np.sin(2*phi)**2 - ts**2)
           * (1 + 2*rs*np.cos(2*phi) + rs**2))
    den = (4*rs**2 * np.sin(2*phi)**2 + rs**2)**2
    Omega = Pbs*gamma/(2*Isql) * num / den

    return C, Sb_vac, D, Omega


def ioLossy(
        ff, phi, zeta, Tsrm, Pbs, Ti=0.01, m=40, Larm=4e3, Lrt=0, epsSR=0,
        epsPD=0, phase=True):
    """Compute lossy BnC input/output relations

    Inputs:
      ff: frequency points [Hz]
      phi: SRC detuning [deg]
      zeta: homodyne angle [deg]
      Tsrm: SRM transmissivity
      Pbs: power on the BS [W]
      Ti: ITM transmissivity (Default: 0.01)
      m: test mass mass [kg] (Default: 40 kg)
      Larm: arm length [m] (Default: 4 km)
      Lrt: round trip loss in the arm cavities (Default: 0)
      epsSR: SRM loss (Default: 0)
      epsPD: photo detection loss (Default: 0)
      phase: if True multiply the appropriate IO matrices by np.exp(2j*beta)
        (Default: True)

    Returns:
      C: the IO matrix from in vacuum to out vacuum
      Sb_vac: the (non-signal referred) quantum *amplitude* spectral density
      D: the IO matrix from mirror strain to out vacuum
      P: the IO matrix from SRM loss vacuum to out vacuum
      Q: the IO matrix from PD loss vacuum to out vacuum
      N: the IO matrix from arm loss vacuum to out vacuum

    Note that all IO vacuum matrices have shape (2, 2, len(ff))
    (or (2, 2) if ff is a single frequency point),
    and the IO signal matrix has shape (2, len(ff))
    (or (2,) if ff is a single frequency point).
    """
    # Compute some basic quantities
    phi = phi * np.pi/180
    zeta = zeta * np.pi/180
    ww = -2*np.pi*ff
    w0 = 2*np.pi*scc.c / 1064e-9
    gamma = scc.c*Ti / (4*Larm)
    beta = np.arctan(ww/gamma)
    ts = np.sqrt(Tsrm)
    rs = np.sqrt(1 - Tsrm)

    # IO quantities
    Isql = m * Larm**2 * gamma**4 / (4*w0)
    hsql = np.sqrt(8*scc.hbar / (m * ww**2 * Larm**2))
    K = 2*Pbs/Isql * gamma**4 / (ww**2 * (gamma**2 + ww**2))
    eps = 2*Lrt/Ti
    cKs = np.cos(2*phi) + K/2 * np.sin(2*phi)
    ejb = np.exp(2j*beta)

    ######################################################################
    # Normalization
    ######################################################################
    M = (1 + rs**2 * np.exp(4j*beta) - 2*rs*ejb * cKs)
    M += epsSR*rs*ejb*(-rs*ejb + cKs)
    M += eps*rs*(2*np.cos(beta)**2 * (-rs*ejb + np.cos(2*phi))
                 + K/2*(3 + ejb)*np.sin(2*phi))*np.exp(2j*beta)

    ######################################################################
    # IO matrix
    ######################################################################
    C11 = np.sqrt(1 - epsPD)*(
        (1 + rs**2)*cKs - 2*rs*np.cos(2*beta)
        - eps/4*(
            -2*rs*(1 + ejb)**2 + 4*(1 + rs**2)*np.cos(beta)**2*np.cos(2*phi)
            + (3 + ejb)*K*(1 + rs**2)*np.sin(2*phi))
        + epsSR*(ejb*rs - 1/2*(1 + rs**2)*cKs))

    C12 = np.sqrt(1 - epsPD)*ts**2 * (
        -(np.sin(2*phi) + K*np.sin(phi)**2)
        + eps/2*np.sin(phi)*((3 + ejb)*K*np.sin(phi)
                             + 4*np.cos(beta)**2 * np.cos(phi))
        + epsSR/2*(np.sin(2*phi) - K*np.sin(phi)**2))

    C21 = np.sqrt(1 - epsPD)*ts**2 * (
        (np.sin(2*phi) - K*np.cos(phi)**2)
        + eps/2*np.cos(phi)*((3 + ejb)*K*np.cos(phi)
                             - 4*np.cos(beta)**2 * np.sin(phi))
        + epsSR/2*(-np.sin(2*phi) + K*np.cos(phi)**2))

    C = np.array([[C11, C12], [C21, C11]]) / M
    if phase:
        C = ejb * C

    ######################################################################
    # transfer function
    ######################################################################
    D1 = np.sqrt(1 - epsPD)*(
        -(1 + rs*ejb)*np.sin(phi)
        + eps/4*np.sin(phi)*(3 + rs + 2*rs*np.exp(4j*beta) + ejb*(1 + 5*rs))
        + epsSR*ejb*rs*np.sin(phi))

    D2 = np.sqrt(1 - epsPD)*(
        -(-1 + rs*ejb)*np.cos(phi)
        + eps/4*np.cos(phi)*(-3 + rs + 2*rs*np.exp(4j*beta) + ejb*(-1 + 5*rs))
        + epsSR*ejb*rs*np.cos(phi))

    D = np.sqrt(2*K)*ts*np.exp(1j*beta)/(hsql*M) * np.array([D1, D2])

    ######################################################################
    # IO SRM losses
    ######################################################################
    P11 = np.sqrt(1 - epsPD)*np.sqrt(epsSR)*ts/2 * (
        -2*rs*ejb + 2*np.cos(2*phi) + K*np.sin(2*phi))

    P12 = -np.sqrt(1 - epsPD)*np.sqrt(epsSR)*ts*np.sin(phi)*(2*np.cos(phi)
                                                             + K*np.sin(phi))

    P21 = np.sqrt(1 - epsPD)*np.sqrt(epsSR)*ts*np.cos(phi)*(2*np.sin(phi)
                                                            - K*np.cos(phi))

    P = np.array([[P11, P12], [P21, P11]]) / M
    if phase:
        P = ejb * P

    ######################################################################
    # IO PD losses
    ######################################################################
    Q11 = np.sqrt(epsPD)*(
        1/ejb + rs**2 * ejb - rs*(2*np.cos(2*phi) + K*np.sin(2*phi))
        + eps*rs/2*(1/ejb*np.cos(2*phi)
                    + ejb*(-2*rs - 2*rs*np.cos(2*beta) + np.cos(2*phi)
                           + K*np.sin(2*phi))
                    + 2*np.cos(2*phi) + 3*K*np.sin(2*phi))
        - epsSR*rs/2*(2*rs*ejb - 2*np.cos(2*phi) - K*np.sin(2*phi)))

    Q12 = np.zeros_like(Q11)

    Q = np.array([[Q11, Q12], [Q12, Q11]]) / M
    if phase:
        Q = ejb * Q

    ######################################################################
    # IO arm losses
    ######################################################################
    N11 = np.sqrt(1 - epsPD)*np.sqrt(eps/2)*ts*(
        K*(1 + rs*ejb)*np.sin(phi)
        + 2*np.cos(beta)*(np.exp(-1j*beta)*np.cos(phi)
                          - rs*np.exp(1j*beta)*(np.cos(phi) + K*np.sin(phi))))

    N12 = -np.sqrt(1 - epsPD)*np.sqrt(2*eps)*ts*(
        np.exp(-1j*beta) + rs*np.exp(1j*beta))*np.cos(beta)*np.sin(phi)

    N21 = np.sqrt(1 - epsPD)*np.sqrt(eps/2)*ts*(
        -K*(1 + rs)*np.cos(phi)
        + 2*np.cos(beta)*(np.exp(-1j*beta) + rs*np.exp(1j*beta))
        * np.cos(beta)*np.sin(phi))

    N22 = -np.sqrt(1 - epsPD)*np.sqrt(2*eps)*ts*(
        -np.exp(-1j*beta) + rs*np.exp(1j*beta))*np.cos(beta)*np.cos(phi)

    N = np.array([[N11, N12], [N21, N22]]) / M
    if phase:
        N = ejb * N

    ######################################################################
    # ASD
    ######################################################################
    Sb_vac = np.sqrt(np.abs(C[0, 0]*np.sin(zeta) + C[1, 0]*np.cos(zeta))**2
                     + np.abs(C[0, 1]*np.sin(zeta) + C[1, 1]*np.cos(zeta))**2
                     + np.abs(P[0, 0]*np.sin(zeta) + P[1, 0]*np.cos(zeta))**2
                     + np.abs(P[0, 1]*np.sin(zeta) + P[1, 1]*np.cos(zeta))**2
                     + np.abs(Q[0, 0]*np.sin(zeta) + Q[1, 0]*np.cos(zeta))**2
                     + np.abs(Q[0, 1]*np.sin(zeta) + Q[1, 1]*np.cos(zeta))**2
                     + np.abs(N[0, 0]*np.sin(zeta) + N[1, 0]*np.cos(zeta))**2
                     + np.abs(N[0, 1]*np.sin(zeta) + N[1, 1]*np.cos(zeta))**2)

    return C, Sb_vac, D, P, Q, N


def convParams2effParams(phi, Tsrm, Pbs, Ti, m, Larm):
    """Convert parameters of a DRFPMI to an effective 3 mirror cavity

    Converts the parameters of a standard DRFPMI to an effective 3 mirror
    cavity as described in.

    Inputs:
      phi: SRC detuning [deg]
      Tsrm: SRM transmissivity
      Pbs: Power on BS [W]
      Ti: ITM transmisivity
      m: test mass mass [kg]
      Larm: arm length [m]

    Returns:
      lam: Free optical resonance frequency [Hz]
      eps: Free optical decay rate [Hz]
      ic: Scaled arm power [Hz^3]
    """
    # Compute some basic quantities
    phi = phi*np.pi/180
    gamma = scc.c*Ti/(4*Larm)  # cavity pole
    rs = np.sqrt(1 - Tsrm)  # SRM reflectivity
    Pc = 2*Pbs/Ti  # Circulating power
    w0 = 2*np.pi*scc.c / 1064e-9  # laser frequency

    # Effective scalled parameters
    ic = 8*w0*Pc/(m*Larm*scc.c)
    den = 1 + rs**2 + 2*rs*np.cos(2*phi)
    lam = 2*rs*gamma*np.sin(2*phi) / den
    eps = (1 - rs**2)*gamma / den

    return lam, eps, ic


def io3MirrorEff(ff, zeta, lam, eps, ic, m, Larm):
    """IO relations for the effective 3 mirror cavity
    """
    # Compute some basic quantities
    zeta = zeta * np.pi/180
    ww = -2*np.pi*ff
    hsql = np.sqrt(8*scc.hbar / (m * ww**2 * Larm**2))

    # Normalization
    M = (lam**2 - (ww + 1j*eps)**2)*ww**2 - lam*ic

    # IO matrix
    C11 = ww**2 * (ww**2 - lam**2 + eps**2) + lam*ic
    C12 = -2*eps*lam*ww**2
    C21 = 2*eps*lam*ww**2 - 2*eps*ic
    C = np.array([[C11, C12], [C21, C11]]) / M

    # transfer function
    D1 = -2*lam*np.sqrt(eps*ic)*ww
    D2 = 2*(eps - 1j*ww)*ww*np.sqrt(eps*ic)
    D = np.array([D1, D2]) / (M*hsql)

    # ASD
    Sb_vac = np.sqrt(np.abs(C[0, 0]*np.sin(zeta) + C[1, 0]*np.cos(zeta))**2
                     + np.abs(C[0, 1]*np.sin(zeta) + C[1, 1]*np.cos(zeta))**2)

    return C, Sb_vac, D


def ioHet(
        ff, phi, Tsrm, Pbs, phid, Dp, Dm, fm, Ti=0.01, m=40, Larm=4e3,
        Lrt=0, epsSR=0, epsPD=0):
    """Compute lossy BnC input/output relations for heterodyne detection

    Inputs:
      ff: frequency points [Hz]
      phi: SRC detuning [deg]
      Tsrm: SRM transmissivity
      Pbs: power on the BS [W]
      Dp: amplitude of the positive sideband
      Dm: amplitude of the lower sideband
      phid: heterodyne angle [deg]
      fm: frequency of RF sideband [Hz]
      Ti: ITM transmissivity (Default: 0.01)
      m: test mass mass [kg] (Default: 40 kg)
      Larm: arm length [m] (Default: 4 km)
      Lrt: round trip loss in the arm cavities (Default: 0)
      epsSR: SRM loss (Default: 0)
      epsPD: photo detection loss (Default: 0)
      phase: if True multiply the appropriate IO matrices by np.exp(2j*beta)
        (Default: True)

    Returns:
      C: the IO matrix from in vacuum to out vacuum
      Sb_vac: the (non-signal referred) quantum *amplitude* spectral density
      Sb_add: the (non-signal referred) *amplitude* spectral density of the
        additional noise from the heterodyne detection
      D: the IO matrix from mirror strain to out vacuum
      P: the IO matrix from SRM loss vacuum to out vacuum
      Q: the IO matrix from PD loss vacuum to out vacuum
      N: the IO matrix from arm loss vacuum to out vacuum

    Note that all IO vacuum matrices have shape (2, 2, len(ff))
    (or (2, 2) if ff is a single frequency point),
    and the IO signal matrix has shape (2, len(ff))
    (or (2,) if ff is a single frequency point).
    """
    phid = phid * np.pi/180
    DpDm = Dp*np.exp(-1j*phid) + Dm*np.exp(1j*phid)
    # Convert heterodyne angle into homodyne angle
    zeta = np.pi/2 + np.angle(DpDm)
    # get standard homodyne IO relations
    C, Sb_vac, D, P, Q, N = ioLossy(
        ff, phi, zeta, Tsrm, Pbs, Ti, m, Larm, Lrt, epsSR, epsPD)
    # compute additional heterodyne noise
    # note that this is an ASD and not a PSD
    Sb_add = np.sqrt((np.abs(Dp)**2 + np.abs(Dm)**2)/np.abs(DpDm)**2)
    return C, Sb_vac, Sb_add, D, P, Q, N


def arccot(x): return np.pi/2 - np.arctan(x)


def dBsqz(C, eps=0):
    '''
    Returns squeeze factor at the optimum homodyne angle
    Input:
      C: IO matrix
    Returns:
      r: squeeze factor [dB]
      zeta: optimum homodyne angle [deg]
    '''
    if eps < 0 or eps > 1:
        raise ValueError('eps must be in [0, 1]')
    S1 = (1 - eps) * (np.abs(C[0, 0])**2 + np.abs(C[0, 1])**2) + eps
    S2 = (1 - eps) * (np.abs(C[1, 0])**2 + np.abs(C[1, 1])**2) + eps
    S12 = (1 - eps) * (
        np.real(np.conj(C[0, 0]) * C[1, 0] + np.conj(C[0, 1]) * C[1, 1]))
    r = 1/2 * np.arcsinh(1/2 * np.sqrt((S1 - S2)**2 + 4*S12**2))
    zeta = 1/2 * arccot((S2 - S1)/(2*S12))
    # Compute second derivative of noise
    # If it's negative this is a maximum instead of a minimum
    # So rotate by pi/2 to get to a minimum
    ddS = 2*np.cos(2*zeta) * (S1 - S2) - 4*np.sin(2*zeta) * S12
    if len(C.shape) == 2:
        if ddS < 0:
            zeta += np.pi / 2
    elif len(C.shape) == 3:
        inds = ddS < 0
        zeta[inds] += np.pi/2
    return 20*r/np.log(10), zeta*180/np.pi


def ioNew(ff, phi, zeta, Tsrm, Pbs, Ti=0.01, m=40, Larm=4e3):
    """I/O Relations for new variables
    """
    phi = np.pi*phi/180
    zeta = np.pi*zeta/180
    fa = Ti*scc.c/(8*np.pi*Larm)
    rs = np.sqrt(1 - Tsrm)
    ts = np.sqrt(Tsrm)
    lambda0 = 1064e-9
    xx = ff/fa
    alpha = 1/2 * Pbs*scc.c/(np.pi**3 * lambda0 * m * Larm**2 * fa**4)
    K = 2*alpha/(xx**2 * (1 + xx**2))

    # homodyne zero Eq.(2)
    cp = np.cos(phi + zeta)
    cm = np.cos(phi - zeta)
    c2p = np.cos(2*phi)
    s2p = np.sin(2*phi)
    c2z = np.cos(2*zeta)
    s2z = np.sin(2*zeta)
    cp2 = np.cos(phi)**2
    sp2 = np.sin(phi)**2
    cz2 = np.cos(zeta)**2
    sz2 = np.sin(zeta)**2
    z = fa * (cp - rs*cm)/(cp + rs*cm)

    # pole Eqs.(3) and (4)
    rcp = 2*rs*np.cos(2*phi)
    rsp = 2*rs*np.sin(2*phi)
    abs_p = fa * np.sqrt((1 - rcp + rs**2)/(1 + rcp + rs**2))
    Q_p = 0.5 * np.sqrt(1 - 2*rs**2 * np.cos(4*phi) + rs**4)/(1 - rs**2)

    # spring frequency Eq.(5)
    xi2 = fa**2 * rsp/(1 - rcp + rs**2)
    xi2 = xi2 * scc.c*Pbs/(2*np.pi**3 * fa**4 * m*Larm**2 * lambda0)

    # denominator of exp(2j*beta) / M / (1 + x**2)
    pref = (1 + xx**2) \
        / ((1 + 1j*ff/(abs_p*Q_p) - ff**2/abs_p**2 - xi2/ff**2)
           * (1 - rcp + rs**2))

    C11 = (1 + rs**2)*(c2p + K/2*s2p) - 2*rs*(1 - xx**2)/(1 + xx**2)
    C21 = ts**2 * (s2p - K*np.cos(phi)**2)
    C12 = -ts**2 * (s2p + K*np.sin(phi)**2)
    C = np.array([[C11, C12], [C21, C11]]) * pref

    # ASD
    SS1 = C11**2 - s2z*C11*ts**2*K * (1 + xx**2)
    # SS2 = C12**2 * np.sin(zeta)**2 + C21**2 * np.cos(zeta)**2
    SS2 = ((1 + xx**2)*ts**2)**2 * (
        s2p**2 +
        + K**2/4 * (cm**2 + cp**2 + 2*cm*cp*c2p)
        - 2*K*s2p*np.cos(phi - zeta)*np.cos(phi + zeta))
    #    + K**2 * (cp2**2*cz2 + sp2**2*sz2)
    Sb_vac = SS1 + SS2

    # Optimal homodyne angle
    zmax = 1/2 * arccot(ts**2 * (2*s2p - K*c2p)/(2*C11))

    return C, np.sqrt(Sb_vac)/np.abs(pref), pref, zmax*180/np.pi


def computeTF(ff, phi, zeta, Tsrm, Pbs, Ti=0.01, m=40, Larm=4e3, g=1):
    '''Compute DARM transfer function and its parameters

    Calculates the homodyne zero, pole, spring frequency, and full DARM
    transfer function as defined in arXiv:1712.09719

    Inputs:
      ff: frequency points [Hz]
      phi: SRC detuning [deg]
      zeta: homodyne angle [deg]
      Tsrm: SRM transmissivity
      Pbs: power on the BS [W]
      Ti: ITM transmissivity (Default: 0.01)
      m: test mass mass [kg] (Default: 40 kg)
      Larm: arm length [m] (Default: 4 km)
      g: optical gain [W/m] (Default: 1 W/m)

    Returns:
      tf: the transfer function [W/m]
      z: the homodyne zero [Hz]
      abs_p: the magnitude of the pole [Hz]
      Q_p: the Q factor
      xi2: the squared spring frequency [Hz**2]
    '''
    phi = np.pi*phi/180
    zeta = np.pi*zeta/180
    fa = Ti*scc.c/(8*np.pi*Larm)
    rs = np.sqrt(1 - Tsrm)
    lambda0 = 1064e-9

    # homodyne zero Eq.(2)
    cp = np.cos(phi + zeta)
    cm = np.cos(phi - zeta)
    z = fa * (cp - rs*cm)/(cp + rs*cm)

    # pole Eqs.(3) and (4)
    rcp = 2*rs*np.cos(2*phi)
    rsp = 2*rs*np.sin(2*phi)
    abs_p = fa * np.sqrt((1 - rcp + rs**2)/(1 + rcp + rs**2))
    Q_p = 0.5 * np.sqrt(1 - 2*rs**2 * np.cos(4*phi) + rs**4)/(1 - rs**2)

    # spring frequency Eq.(5)
    xi2 = fa**2 * rsp/(1 - rcp + rs**2)
    xi2 = xi2 * scc.c*Pbs/(2*np.pi**3 * fa**4 * m*Larm**2 * lambda0)

    # TF Eq.(6)
    num = g*np.exp(-2*np.pi*1j*ff*Larm/scc.c) * (1 + 1j*ff/z)
    den = 1 + 1j*ff/(abs_p*Q_p) - ff**2/abs_p**2 - xi2/ff**2
    pref = (cp - rs*cm)/(1 - rcp + rs**2)
    tf = pref * num/den
    return tf, z, abs_p, Q_p, xi2, pref


def computeIOsmallSR(ff, phi, zeta, Tsrm, Pbs, Ti=0.01, m=40, Larm=4e3):
    """Compute relations for nearly SR
    phi: small SRC detuning
    """
    phi = np.pi*phi/180
    zeta = np.pi*zeta/180
    fa = Ti*scc.c/(8*np.pi*Larm)
    rs = np.sqrt(1 - Tsrm)
    ts = np.sqrt(Tsrm)
    lambda0 = 1064e-9
    xx = ff/fa
    alpha = 1/2 * Pbs*scc.c/(np.pi**3 * lambda0 * m * Larm**2 * fa**4)
    K = 2*alpha/(xx**2 * (1 + xx**2))

    # pole Eqs.(3) and (4)
    abs_p = fa * (1 - rs)/(1 + rs)
    Q_p = 0.5

    # spring frequency Eq.(5)
    xi2 = fa**2 * 4*rs*phi/(1 - rs)**2
    xi2 = xi2 * scc.c*Pbs/(2*np.pi**3 * fa**4 * m*Larm**2 * lambda0)

    # denominator of exp(2j*beta) / M
    # pref = ((1 + 1j*ff/(abs_p*Q_p) - ff**2/abs_p**2 - xi2/ff**2)
    #         * (1 - rs)**2)
    pref = ((1 + xx**2) / (1 - rs)**2) \
        / ((1 + 2j*ff/abs_p - ff**2/abs_p**2 - xi2/ff**2))

    C11 = (1 + rs**2)*(1 + K*phi) - 2*rs*(1 - xx**2)/(1 + xx**2)
    C21 = ts**2 * (2*phi - K)*np.ones_like(ff)
    C12 = -2*ts**2 * phi * np.ones_like(ff)
    C = np.array([[C11, C12], [C21, C11]]) * pref

    # Optimal homodyne angle
    zeta_max = 1/2 * arccot(ts**2/(2*C11) * (4*phi - K))
    zeta_max *= 180/np.pi

    return C, abs_p, Q_p, xi2, pref, zeta_max


def computeIOsmallRSE(ff, phi, zeta, Tsrm, Pbs, Ti=0.01, m=40, Larm=4e3):
    """Compute relations for nearly ESR or ERSE configurations
    phi is small difference from pi/2: phi_src = pi/2 + phi
    """
    phi = np.pi*phi/180
    zeta = np.pi*zeta/180
    fa = Ti*scc.c/(8*np.pi*Larm)
    rs = np.sqrt(1 - Tsrm)
    ts = np.sqrt(Tsrm)
    lambda0 = 1064e-9
    xx = ff/fa
    alpha = 1/2 * Pbs*scc.c/(np.pi**3 * lambda0 * m * Larm**2 * fa**4)
    K = 2*alpha/(xx**2 * (1 + xx**2))

    # pole Eqs.(3) and (4)
    abs_p = fa * (1 + rs)/(1 - rs)
    Q_p = 0.5

    # spring frequency Eq.(5)
    xi2 = fa**2 * 4*rs*phi/(1 + rs)**2
    xi2 = -xi2 * scc.c*Pbs/(2*np.pi**3 * fa**4 * m*Larm**2 * lambda0)

    # denominator of exp(2j*beta) / M
    # pref = ((1 + 1j*ff/(abs_p*Q_p) - ff**2/abs_p**2 - xi2/ff**2)
    #         * (1 - rs)**2)
    pref = ((1 + xx**2) / (1 + rs)**2) \
        / ((1 + 2j*ff/abs_p - ff**2/abs_p**2 - xi2/ff**2))

    C11 = -(1 + rs**2)*(1 + K*phi) - 2*rs*(1 - xx**2)/(1 + xx**2)
    C12 = ts**2 * (2*phi - K)*np.ones_like(ff)
    C21 = -2*ts**2 * phi * np.ones_like(ff)
    C = np.array([[C11, C12], [C21, C11]]) * pref

    # Optimal homodyne angle
    zeta_max = -1/2 * arccot(ts**2/(2*C11) * (4*phi - K))
    zeta_max *= 180/np.pi

    return C, abs_p, Q_p, xi2, pref, zeta_max


def plotNoiseEllipse(C, eps=0):
    """Plot Noise Ellipse
    """
    fig = plt.figure()
    tt = 2*np.pi*np.linspace(0, 1, 100)
    # I had np.sqrt(2)*cos(tt) and sin(tt) before. Why?
    a1 = np.cos(tt)
    a2 = np.sin(tt)
    b1 = np.sqrt(1 - eps) * C[0, 0] * a1 + C[0, 1] * a2 + np.sqrt(eps)
    b2 = np.sqrt(1 - eps) * C[1, 0] * a1 + C[1, 1] * a2 + np.sqrt(eps)
    S1 = np.abs(C[0, 0])**2 + np.abs(C[0, 1])**2
    S2 = np.abs(C[1, 1])**2 + np.abs(C[1, 0])**2
    S12 = np.real(np.conj(C[0, 0])*C[1, 0] + np.conj(C[0, 1])*C[1, 1])
    # zeta = (C[1, 1]**2 + C[1, 0]**2 - C[0, 1]**2 - C[0, 0]**2)
    # zeta = zeta / (2*(C[0, 0]*C[1, 0] + C[0, 1]*C[1, 1]))
    # zeta = np.pi/2 - 1/2 * arccot(zeta)
    # # zeta = np.pi/2 - 1/2 * np.arctan(1/zeta)
    zeta = 1/2 * arccot((S2 - S1)/(2*S12))
    # Compute second derivative of noise
    ddS = 2*np.cos(2*zeta) * (S1 - S2) - 4*np.sin(2*zeta) * S12
    if ddS < 0:
        # If it's negative this is a maximum instead of a minimum
        # So rotate by pi/2 to get to a minimum
        zeta += np.pi/2
    zeta = np.pi/2 - zeta  # Correct for BnC conventions
    xx = np.linspace(0, np.cos(zeta))
    yy = np.linspace(0, np.sin(zeta))
    fig.gca().plot(a1, a2, 'C3-.')
    fig.gca().plot(b1, b2, 'C0-')
    # fig.gca().plot(xx, yy, 'C1:', alpha=0.8)
    fig.gca().set_aspect('equal')
    return fig


# def plotNoiseEllipseLossy(C, P, Q, N, fig):
#     # fig = plt.figure()
#     tt = 2*np.pi*np.linspace(0, 1, 100)
#     # I had np.sqrt(2)*cos(tt) and sin(tt) before. Why?
#     a1 = np.cos(tt)
#     a2 = np.sin(tt)
#     b1 = (C[0, 0] + P[0, 0]) * a1 + (C[0, 1] + P[0, 1]) * a2
#     b2 = (C[1, 0] + P[1, 0]) * a1 + (C[1, 1] + P[1, 1]) * a2
#     S1 = np.abs(C[0, 0])**2 + np.abs(C[0, 1])**2
#     S2 = np.abs(C[1, 1])**2 + np.abs(C[1, 0])**2
#     S12 = np.real(np.conj(C[0, 0])*C[1, 0] + np.conj(C[0, 1])*C[1, 1])
#     # zeta = (C[1, 1]**2 + C[1, 0]**2 - C[0, 1]**2 - C[0, 0]**2)
#     # zeta = zeta / (2*(C[0, 0]*C[1, 0] + C[0, 1]*C[1, 1]))
#     # zeta = np.pi/2 - 1/2 * arccot(zeta)
#     # # zeta = np.pi/2 - 1/2 * np.arctan(1/zeta)
#     zeta = 1/2 * arccot((S2 - S1)/(2*S12))
#     # Compute second derivative of noise
#     ddS = 2*np.cos(2*zeta) * (S1 - S2) - 4*np.sin(2*zeta) * S12
#     if ddS < 0:
#         # If it's negative this is a maximum instead of a minimum
#         # So rotate by pi/2 to get to a minimum
#         zeta += np.pi/2
#     zeta = np.pi/2 - zeta  # Correct for BnC conventions
#     xx = np.linspace(0, np.cos(zeta))
#     yy = np.linspace(0, np.sin(zeta))
#     fig.gca().plot(a1, a2, 'C3-', alpha=0.2)
#     fig.gca().plot(b1, b2, 'C0-')
#     fig.gca().plot(xx, yy, 'C1:', alpha=0.8)
#     fig.gca().set_aspect('equal')
#     return fig


def plotLogNoiseEllipse(C, r_offset, eps=0, ax=None, numpts=1000, **kwargs):
    '''Plots the quantum noise ellipse on a polar plot with log radius

    Returns a logarithmic polar plot with radius given by the magnitude
    of the noise in dBvac. Since this can be negative, an offset is added
    to make all radii on the plot positive, but the axes are relabelled to
    reflect the correct values

    BnC conventions are used for the angles, which are computed from C.

    Note that the equation for an ellipse with semi-major axis a and semi-minor
    axis-b is
    \[r = \frac{ab}{\sqrt{b^2\cos^2\theta + a^2\sin^2\theta}}\]
    In this case, $a=e^{r_sqz} = 1/b$ so the radius for the plot, before
    accounting for angles, is
    \[r_db = -10\log_10(e^{2r_sqz}\cos^2\theta + \e^{-2r_sqz}\sin^2\theta)\]

    Input:
      C: a (nfreq, nfreq) array of input output coefficients
      r_offset: offset radius [dB]
      eps: effective losses to add (default: 0)
      ax: axis to plot on. If none, new axis is returned (default: None)
      numpts: number of points on the plot (default: 1000)
      **kwargs: extra arguments to pass to the plot command

    Returns:
      ax: the axis for the plot
    '''
    rsqz, zeta = dBsqz(C, eps=eps)
    sqzAmp = 10**(rsqz/20)
    zeta = np.pi/2 - zeta*np.pi/180  # correct for BnC conventions
    # zeta = zeta*np.pi/180
    tt = 2*np.pi*np.linspace(0, 1, numpts)
    rdb = -10*np.log10(
        (sqzAmp*np.cos(tt - zeta))**2
        + (1/sqzAmp*np.sin(tt - zeta))**2) + r_offset
    if ax is None:
        ax = plt.subplot(111, projection='polar')
    ax.plot(tt, rdb, **kwargs)
    ax.plot(tt, np.ones_like(tt)*r_offset, 'C3-.')
    if 'label' in kwargs.keys():
        del kwargs['label']
    if 'ls' in kwargs.keys():
        del kwargs['ls']
    # ax.plot(zeta*np.ones(50), np.linspace(0, r_offset), ls=':',
    #         alpha=0.8, **kwargs)
    yticks = (np.array(ax.get_yticks()) - r_offset).astype(int)
    ax.set_yticklabels(yticks.astype(str))
    xlabels = np.arange(-180, 200, 45)[1:]
    xlabels = [r'${:s}^\circ$'.format(lab) for lab in xlabels.astype(str)]
    ax.set_xticklabels(np.roll(xlabels[::-1], -2))
    return ax


def plotLogNoiseEllipse2(C, r_offset, eps=0, ax=None, numpts=1000, rmax=None):
    '''Plots the quantum noise ellipse on a polar plot with log radius

    Like plotLogNoiseEllipse except can set maximum radius

    Returns:
      ax: the axis for the plot
    '''
    rsqz, zeta = dBsqz(C, eps=eps)
    sqzAmp = 10**(rsqz/20)
    zeta = np.pi/2 - zeta*np.pi/180  # correct for BnC conventions
    # zeta = zeta*np.pi/180
    tt = 2*np.pi*np.linspace(0, 1, numpts)
    rdb = -10*np.log10(
        (sqzAmp*np.cos(tt - zeta))**2
        + (1/sqzAmp*np.sin(tt - zeta))**2) + r_offset
    if ax is None:
        ax = plt.subplot(111, projection='polar')
    ax.plot(tt, rdb)
    ax.plot(tt, np.ones_like(tt)*r_offset, 'C3-.')
    if rmax is not None:
        ax.set_ylim(0, rmax + r_offset)
    yticks = (np.array(ax.get_yticks()) - r_offset).astype(int)
    ax.set_yticklabels(yticks.astype(str))
    xlabels = np.arange(-180, 200, 45)[1:]
    xlabels = [r'${:s}^\circ$'.format(lab) for lab in xlabels.astype(str)]
    ax.set_xticklabels(np.roll(xlabels[::-1], -2))
    return ax


def plotLogNoiseEllipseLossy(
        C, P, Q, N, r_offset, eps=0, ax=None, numpts=1000, plot_vac=True,
        homo_ang=None, **kwargs):
    '''Plots the quantum noise ellipse on a polar plot with log radius

    Returns a logarithmic polar plot with radius given by the magnitude
    of the noise in dBvac. Since this can be negative, an offset is added
    to make all radii on the plot positive, but the axes are relabelled to
    reflect the correct values

    BnC conventions are used for the angles, which are computed from C.

    Note that the equation for an ellipse with semi-major axis a and semi-minor
    axis-b is
    \[r = \frac{ab}{\sqrt{b^2\cos^2\theta + a^2\sin^2\theta}}\]
    In this case, $a=e^{r_sqz} = 1/b$ so the radius for the plot, before
    accounting for angles, is
    \[r_db = -10\log_10(e^{2r_sqz}\cos^2\theta + \e^{-2r_sqz}\sin^2\theta)\]

    Input:
      C: a (nfreq, nfreq) array of input output coefficients
      r_offset: offset radius [dB]
      eps: effective losses to add (default: 0)
      ax: axis to plot on. If none, new axis is returned (default: None)
      numpts: number of points on the plot (default: 1000)
      **kwargs: extra arguments to pass to the plot command

    Returns:
      ax: the axis for the plot
    '''
    def computeSb(zz):
        Sb_vac = (np.abs(C[0, 0]*np.sin(zz) + C[1, 0]*np.cos(zz))**2
                  + np.abs(C[0, 1]*np.sin(zz) + C[1, 1]*np.cos(zz))**2
                  + np.abs(P[0, 0]*np.sin(zz) + P[1, 0]*np.cos(zz))**2
                  + np.abs(P[0, 1]*np.sin(zz) + P[1, 1]*np.cos(zz))**2
                  + np.abs(Q[0, 0]*np.sin(zz) + Q[1, 0]*np.cos(zz))**2
                  + np.abs(Q[0, 1]*np.sin(zz) + Q[1, 1]*np.cos(zz))**2
                  + np.abs(N[0, 0]*np.sin(zz) + N[1, 0]*np.cos(zz))**2
                  + np.abs(N[0, 1]*np.sin(zz) + N[1, 1]*np.cos(zz))**2)
        return np.sqrt(Sb_vac)

    _, zeta = dBsqz(C, eps=eps)
    zeta = zeta*np.pi/180 - np.pi/2
    aa = computeSb(zeta)
    bb = computeSb(zeta + np.pi/2)
    zeta = np.pi/2 - zeta  # correct for BnC conventions
    # zeta = zeta*np.pi/180
    tt = 2*np.pi*np.linspace(0, 1, numpts)
    rdb = 10*np.log10(
        aa*bb / ((bb*np.cos(tt - zeta))**2 + (aa*np.sin(tt - zeta))**2)) \
        + r_offset
    if ax is None:
        ax = plt.subplot(111, projection='polar')
    ax.plot(tt, rdb, **kwargs)
    if plot_vac:
        ax.plot(tt, np.ones_like(tt)*r_offset, 'C3-.')
    if 'label' in kwargs.keys():
        del kwargs['label']
    if 'ls' in kwargs.keys():
        del kwargs['ls']
    ax.plot((zeta - np.pi/2)*np.ones(50), np.linspace(0, r_offset), ls=':',
            alpha=0.8, **kwargs)
    if homo_ang:
        _, rmax = ax.get_ylim()
        ax.plot(np.pi/2 - homo_ang*np.pi/180*np.ones(50), np.linspace(0, rmax),
                c='k', ls=':')
    yticks = (np.array(ax.get_yticks()) - r_offset).astype(int)
    ax.set_yticklabels(yticks.astype(str))
    xlabels = np.arange(-180, 200, 45)[1:]
    xlabels = [r'${:s}^\circ$'.format(lab) for lab in xlabels.astype(str)]
    ax.set_xticklabels(np.roll(xlabels[::-1], -2))
    return ax


def homAngleTFs2(
        ff, phi, zetas, Tsrm, Pbs, Titm, M, Larm, Lrt, epsSR, epsPD, h_class):
    '''
    zetas: angles [rad]
    '''
    tfs = np.zeros_like(zetas)
    Squants = np.zeros_like(zetas)
    Sclass = np.zeros_like(zetas)
    Stots = np.zeros_like(zetas)
    for zi, zeta in enumerate(zetas):
        _, S, D, _, _, _ = ioLossy(
            ff, phi, zeta*180/np.pi, Tsrm, Pbs, Titm, M, Larm, Lrt, epsSR,
            epsPD)
        tf = np.abs(np.sin(zeta) * D[0] + np.cos(zeta) * D[1])
        tfs[zi] = tf
        Sc = tf*h_class
        Squants[zi] = S
        Sclass[zi] = Sc
        Stots[zi] = np.sqrt(S**2 + Sc**2)
    return tfs, Squants, Sclass, Stots


def homAngleTFs(
        ff, phi, zetas, Tsrm, Pbs, Titm, M, Larm, Lrt, epsSR, epsPD):
    '''
    zetas: angles [rad]
    '''
    tfs = np.zeros_like(zetas)
    Squants = np.zeros_like(zetas)
    for zi, zeta in enumerate(zetas):
        _, S, D, _, _, _ = ioLossy(
            ff, phi, zeta*180/np.pi, Tsrm, Pbs, Titm, M, Larm, Lrt, epsSR,
            epsPD)
        tf = np.abs(np.sin(zeta) * D[0] + np.cos(zeta) * D[1])
        tfs[zi] = tf
        Squants[zi] = S
    return tfs, Squants


def plotLogClassNoiseEllipse(
        ff, phi, Tsrm, Pbs, Titm, M, Larm, classNoise, Lrt, epsSR, epsPD,
        r_offset, ax=None, numpts=1000, **kwargs):
    tt = 2*np.pi*np.linspace(0, 1, numpts)
    tfs, Squants = homAngleTFs(ff, phi, tt, Tsrm, Pbs, Titm, M, Larm,
                               Lrt, epsSR, epsPD)
    # tfs, _ = homAngleTFs(ff, phi, np.pi - tt, Tsrm, Pbs, Titm, M, Larm)
    rdb = -20*np.log10(tfs*classNoise)
    rdb_shift = rdb + r_offset
    if ax is None:
        ax = plt.subplot(111, projection='polar')
    ax.plot(tt, rdb_shift, **kwargs)
    ax.plot(tt, np.ones_like(tt)*r_offset, 'C3-.')
    if 'label' in kwargs.keys():
        del kwargs['label']
    # ax.plot(zeta*np.ones(50), np.linspace(0, r_offset), ls=':',
    #         alpha=0.8, **kwargs)
    yticks = (np.array(ax.get_yticks()) - r_offset).astype(int)
    ax.set_yticklabels(yticks.astype(str))
    xlabels = np.arange(-180, 200, 45)[1:]
    xlabels = [r'${:s}^\circ$'.format(lab) for lab in xlabels.astype(str)]
    ax.set_xticklabels(np.roll(xlabels[::-1], -2))
    return ax


def plotLogHomodyneEllipse(
        ff, phi, Tsrm, Pbs, Titm, M, Larm, Lrt, epsSR, epsPD, r_offset,
        r_max=None, ax=None, numpts=1000, plot_vac=True, homo_ang=None,
        **kwargs):
    '''Plots the quantum noise ellipse on a polar plot with log radius

    Returns a logarithmic polar plot with radius given by the magnitude
    of the noise in dBvac. Since this can be negative, an offset is added
    to make all radii on the plot positive, but the axes are relabelled to
    reflect the correct values

    BnC conventions are used for the angles, which are computed from C.

    Note that the equation for an ellipse with semi-major axis a and semi-minor
    axis-b is
    \[r = \frac{ab}{\sqrt{b^2\cos^2\theta + a^2\sin^2\theta}}\]
    In this case, $a=e^{r_sqz} = 1/b$ so the radius for the plot, before
    accounting for angles, is
    \[r_db = -10\log_10(e^{2r_sqz}\cos^2\theta + \e^{-2r_sqz}\sin^2\theta)\]

    Input:
      C: a (nfreq, nfreq) array of input output coefficients
      r_offset: offset radius [dB]
      eps: effective losses to add (default: 0)
      ax: axis to plot on. If none, new axis is returned (default: None)
      numpts: number of points on the plot (default: 1000)
      **kwargs: extra arguments to pass to the plot command

    Returns:
      ax: the axis for the plot
    '''
    tt = 2*np.pi*np.linspace(0, 1, numpts)
    _, Squants = homAngleTFs(
        ff, phi, np.pi/2 - tt, Tsrm, Pbs, Titm, M, Larm, Lrt, epsSR, epsPD)
    rdb = 20*np.log10(Squants) + r_offset
    if ax is None:
        ax = plt.subplot(111, projection='polar')
    ax.plot(tt, rdb, **kwargs)
    if plot_vac:
        ax.plot(tt, np.ones_like(tt)*r_offset, 'C3-.')
    # if 'label' in kwargs.keys():
    #     del kwargs['label']
    # ax.plot(zeta*np.ones(50), np.linspace(0, r_offset), ls=':',
    #         alpha=0.8, **kwargs)
    if homo_ang:
        _, rmax = ax.get_ylim()
        ax.plot(np.pi/2 - homo_ang*np.pi/180*np.ones(50), np.linspace(0, rmax),
                c='k', ls=':')
    if r_max is not None:
        ax.set_ylim(0, r_max + r_offset)
    yticks = (np.array(ax.get_yticks()) - r_offset).astype(int)
    ax.set_yticklabels(yticks.astype(str))
    xlabels = np.arange(-180, 200, 45)[1:]
    xlabels = [r'${:s}^\circ$'.format(lab) for lab in xlabels.astype(str)]
    ax.set_xticklabels(np.roll(xlabels[::-1], -2))
    return ax


def plotLogHomodyneEllipseClassical(
        ff, phi, Tsrm, Pbs, Titm, M, Larm, Lrt, epsSR, epsPD, Sclass,
        r_offset, r_max=None, ax=None, numpts=1000, **kwargs):
    '''Plots the quantum noise ellipse on a polar plot with log radius

    Returns a logarithmic polar plot with radius given by the magnitude
    of the noise in dBvac. Since this can be negative, an offset is added
    to make all radii on the plot positive, but the axes are relabelled to
    reflect the correct values

    BnC conventions are used for the angles, which are computed from C.

    Note that the equation for an ellipse with semi-major axis a and semi-minor
    axis-b is
    \[r = \frac{ab}{\sqrt{b^2\cos^2\theta + a^2\sin^2\theta}}\]
    In this case, $a=e^{r_sqz} = 1/b$ so the radius for the plot, before
    accounting for angles, is
    \[r_db = -10\log_10(e^{2r_sqz}\cos^2\theta + \e^{-2r_sqz}\sin^2\theta)\]

    Input:
      C: a (nfreq, nfreq) array of input output coefficients
      r_offset: offset radius [dB]
      eps: effective losses to add (default: 0)
      ax: axis to plot on. If none, new axis is returned (default: None)
      numpts: number of points on the plot (default: 1000)
      **kwargs: extra arguments to pass to the plot command

    Returns:
      ax: the axis for the plot
    '''
    tt = 2*np.pi*np.linspace(0, 1, numpts)
    _, _, Sclasses, _ = homAngleTFs2(
        ff, phi, np.pi/2 - tt, Tsrm, Pbs, Titm, M, Larm, Lrt, epsSR, epsPD,
        Sclass)
    rdb = 20*np.log10(Sclasses) + r_offset
    if ax is None:
        ax = plt.subplot(111, projection='polar')
    ax.plot(tt, rdb, **kwargs)
    ax.plot(tt, np.ones_like(tt)*r_offset, 'C3-.')
    # if 'label' in kwargs.keys():
    #     del kwargs['label']
    # ax.plot(zeta*np.ones(50), np.linspace(0, r_offset), ls=':',
    #         alpha=0.8, **kwargs)
    if r_max is not None:
        ax.set_ylim(0, r_max + r_offset)
    yticks = (np.array(ax.get_yticks()) - r_offset).astype(int)
    ax.set_yticklabels(yticks.astype(str))
    xlabels = np.arange(-180, 200, 45)[1:]
    xlabels = [r'${:s}^\circ$'.format(lab) for lab in xlabels.astype(str)]
    ax.set_xticklabels(np.roll(xlabels[::-1], -2))
    return ax


def plotLogHomodyneEllipseAll(
        ff, phi, Tsrm, Pbs, Titm, M, Larm, Lrt, epsSR, epsPD, Sclass,
        r_offset, r_max=None, ax=None, numpts=1000, **kwargs):
    '''Plots the quantum noise ellipse on a polar plot with log radius

    Returns a logarithmic polar plot with radius given by the magnitude
    of the noise in dBvac. Since this can be negative, an offset is added
    to make all radii on the plot positive, but the axes are relabelled to
    reflect the correct values

    BnC conventions are used for the angles, which are computed from C.

    Note that the equation for an ellipse with semi-major axis a and semi-minor
    axis-b is
    \[r = \frac{ab}{\sqrt{b^2\cos^2\theta + a^2\sin^2\theta}}\]
    In this case, $a=e^{r_sqz} = 1/b$ so the radius for the plot, before
    accounting for angles, is
    \[r_db = -10\log_10(e^{2r_sqz}\cos^2\theta + \e^{-2r_sqz}\sin^2\theta)\]

    Input:
      C: a (nfreq, nfreq) array of input output coefficients
      r_offset: offset radius [dB]
      eps: effective losses to add (default: 0)
      ax: axis to plot on. If none, new axis is returned (default: None)
      numpts: number of points on the plot (default: 1000)
      **kwargs: extra arguments to pass to the plot command

    Returns:
      ax: the axis for the plot
    '''
    tt = 2*np.pi*np.linspace(0, 1, numpts)
    _, Squants, Sclasses, Stots = homAngleTFs2(
        ff, phi, np.pi/2 - tt, Tsrm, Pbs, Titm, M, Larm, Lrt, epsSR, epsPD,
        Sclass)
    rdb_q = 20*np.log10(Squants) + r_offset
    rdb_c = 20*np.log10(Sclasses) + r_offset
    # rdb_t = 20*np.log10(Stots) + r_offset
    if ax is None:
        ax = plt.subplot(111, projection='polar')
    if 'ls' in kwargs.keys():
        del kwargs['ls']
    # ax.plot(tt, rdb_q, ls='-', **kwargs)
    # ax.plot(tt, rdb_c, ls='-.', **kwargs)
    if 'c' in kwargs.keys():
        del kwargs['c']
    ax.plot(tt, rdb_q, 'C0-', **kwargs)
    ax.plot(tt, rdb_c, 'C1-.', **kwargs)
    ax.plot(tt, np.ones_like(tt)*r_offset, 'C3-.')
    # if 'label' in kwargs.keys():
    #     del kwargs['label']
    # ax.plot(zeta*np.ones(50), np.linspace(0, r_offset), ls=':',
    #         alpha=0.8, **kwargs)
    if r_max is not None:
        ax.set_ylim(0, r_max + r_offset)
    yticks = (np.array(ax.get_yticks()) - r_offset).astype(int)
    ax.set_yticklabels(yticks.astype(str))
    xlabels = np.arange(-180, 200, 45)[1:]
    xlabels = [r'${:s}^\circ$'.format(lab) for lab in xlabels.astype(str)]
    ax.set_xticklabels(np.roll(xlabels[::-1], -2))
    return ax


def siPrefix(num, tex=False):
    """Write number with an SI prefix

    Input:
      num: the number
      tex: If True the prefix for micro is returned as
        '$\mu$' rather than 'u' for latex formatting
        (Default: False)

    Returns:
      pref: the prefix
      num: the formatted number

    Taken from pytickle
    """
    if num == 0:
        exp = 0
    else:
        exp = np.floor(np.log10(np.abs(num)))
    posPrefixes = ['', 'k', 'M', 'G', 'T']
    negPrefixes = ['m', 'u', 'n', 'p']
    try:
        if np.sign(exp) >= 0:
            ind = int(np.abs(exp) // 3)
            pref = posPrefixes[ind]
            num = num / np.power(10, 3*ind)
        else:
            ind = int((np.abs(exp) - 1) // 3)
            pref = negPrefixes[ind]
            num = num * np.power(10, 3*(ind + 1))
    except IndexError:
        pref = ''
    if tex:
        if pref == 'u':
            pref = r'$\mu$'
    return pref, num
