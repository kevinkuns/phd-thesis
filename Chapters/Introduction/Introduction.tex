\chapter{Introduction}
\label{chap:introduction}

The ground based network of interferometric gravitational wave detectors, consisting of the LIGO observatories in Hanford and Livingston~\cite{Aasi2015} and the Virgo observatory in Italy~\cite{Acernese2015}, have detected ten binary black holes and one binary neutron star to date~\cite{Abbott2016, Abbott2017b, Abbott2018b}. The KAGRA detector in Japan~\cite{Aso2013} will join LIGO and Virgo soon, and construction on a third LIGO site in India will begin shortly. While not as sensitive as these detectors at lower frequencies, the GEO600 detector in Germany~\cite{Affeldt2014} is also part of this global network of detectors.

\section{Future Gravitational Wave Detectors}

With the focus of the current detectors moving from making the first detections to being a network of observatories routinely detecting sources, studies for the future of gravitational wave detectors are underway and focus on improvements to the current network in two ways. First, improving the sensitivity of the next generation of detectors, and, second, expanding the frequency range over which the network can observe. Fig.~\ref{fig:detector-network} shows the sensitivities of possible future detectors.

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{Chapters/Introduction/Figures/Network.pdf}
\caption[Sensitivities of future ground and space gravitational wave detectors]{Sensitivities of future ground and space gravitational wave detectors. The trace labeled GW150914 is the waveform of the first direct detection of gravitational waves.}
\label{fig:detector-network}
\end{figure}

LIGO Voyager is a cryogenic upgrade to the existing LIGO facilities~\cite{VoyagerWhite}, and Cosmic Explorer (CE) would be a 40~km long interferometer~\cite{Abbott2017d, Dwyer2015}. The Einstein Telescope (ET) would be a set of six 10~km long interferometers forming an equilateral triangle~\cite{Hild2011, Punturo2010, Punturo2010b}. Each corner has two interferometers: one cryogenic low-frequency interferometer and one high frequency interferometer.

These ground-based detectors are limited by seismic noise at low frequencies and cannot observe lower than a few Hz in the best case of ET. To see lower frequency sources, one needs to use space detectors which are not affected by these noises and where the arms can be significantly longer than ground-based interferometers.

LISA (Laser Interferometer Space Antenna) is perhaps the best known and furthest developed space detector~\cite{YellowBook}. It consists of three spacecraft in a triangular configuration forming three interferometers with $2\times 10^6\,\t{km}$ long arms. The three satellite constellation is in a heliocentric orbit trailing the Earth by $20^\circ$. It uses time-delay interferometry with a 2~W, 1064~nm laser and 2~kg test masses. LISA is briefly discussed in Chap.~\ref{chap:TianGO}. TianQin is similar to LISA but with $10^5\,\t{km}$ long arms, a 4~W laser, and is in a geocentric orbit~\cite{Luo2016}. DECIGO is another three spacecraft constellation in a heliocentric Earth trailing orbit, but it has 1000~km long arms and does traditional interferometry~\cite{Sato2017, Seto2001}. DECIGO's arms are Fabry-Perot cavities (with a finesse of 10) and it uses a 10~W, 515~nm laser with 100~kg test masses.

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{Chapters/Introduction/Figures/horizons.pdf}
\caption[Compact binary horizons for different ground and space detectors]{Compact binary horizons for different ground and space detectors. The binary systems are equal mass and oriented face on. The maximum detectable distance, defined as the distance at which a source has an SNR of 8 in a given detector, is computed for 48 source locations uniformly tiling the sky. The horizon is the maximum distance at which the best source is detected, 50\% of these sources are detected within the dark shaded band, and 90\% of the sources are detected within the light shaded band. If a source stays in a space detector's sensitivity band for more than 5 years, the 5 year portion of the system's evolution that gives the best SNR in each detector is used.}
\label{fig:horizons}
\end{figure}

TianGO is a relatively simple and cheap space detector and is described in Chap.~\ref{chap:TianGO}. TianGO has three satellites with 100~km long arms that form a simple Michelson interferometer and which will also likely be in a heliocentric Earth trailing orbit. It uses a 5~W, 532~nm laser with 10~kg test masses and employs 10~dB of phase squeezing to reduce quantum noise. Advanced TianGO (aTianGO) is a speculative idea that would use large lightweight foldable mirrors that would allow it to have 1000~km long arms.

The astrophysical reach of most of the detectors shown in Fig.~\ref{fig:detector-network} to compact binary systems is shown in Fig.~\ref{fig:horizons} as a function of the total mass of the binary system. The ground detectors are sensitive to lower mass systems because they are sensitive to higher frequencies than the space detectors. The horizon is the furthest distance a given detector could see an optimally oriented source in the optimal orientation in the sky. The figure also shows how far a given detector can see binary systems that are not at the optimal orientation in the sky. A source is said to be detectable if it has a signal to noise ratio (SNR) of at least 8 in a given detector.

\section{Quantum Noise in Gravitational Wave Detectors}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{Chapters/Introduction/Figures/Voyager_NB.pdf}
\caption[Voyager noise budget]{Voyager noise budget. The interferometer will be dominated by quantum noise from $20\,\t{Hz}$ and above.}
\label{fig:VoyagerNB}
\end{figure}

All of the detectors shown in Fig.~\ref{fig:detector-network} will be limited by quantum shot noise at high frequencies, and some will be limited by quantum radiation pressure at low frequencies. The noise budget for LIGO Voyager~\cite{VoyagerWhite} is shown in Fig.~\ref{fig:VoyagerNB} and shows that the detector will be limited by quantum noise at all frequencies above $20\,\t{Hz}$, though coating Brownian noise makes a significant contribution around 100~Hz. Since quantum noise is so important, much effort is made to use squeezed states of light in interferometers to reduce this noise. The relevant aspects of quantum noise are briefly reviewed in Sec.~\ref{sec:quantum-noise-review}.

These fragile quantum states thus need to be protected from losses and other classical noises. A good way to do this is to get the interferometer to amplify the signal, and noise, itself through optomechanical interactions, known as ponderomotive squeezing~\cite{Corbitt2006}, before the signal encounters the large sources of noise and losses. Sec.~\ref{sec:optomechanical} introduces ponderomotive squeezing and Sec.~\ref{sec:classical-noise} discusses the detrimental effects of losses and classical noise on quantum states and their mitigation with optomechanical amplifiers. Chap.~\ref{chap:40m} discusses progress towards observing ponderomotive squeezing in a large scale 40~m interferometer.

Finally, the source of quantum noise in gravitational wave detectors is the beating of vacuum fluctuations of the electromagnetic field with the strong laser, known as the local oscillator (LO), needed to detect the signals. Chap.~\ref{chap:hypotest} describes a scheme to remove the LO, and thus be free from the quantum noise of the vacuum fluctuations, when looking for deviations from general relativity (GR). To do so, the interferometer mirrors are driven in such a way that the predicted signal from general relativity is canceled; any signal exiting the interferometer thus signals a modification to GR. By counting photons instead of measuring the signal, the need for a local oscillator is removed and the noise due to the vacuum fluctuations is eliminated.

% \section{A Note about Power Spectral Densities}

% Throughout this thesis we exclusively use single-sided power spectral densities. So $S_h^{1/2}$ is strain noise with units of $\t{strain}/\sqrt{\t{Hz}}$, $S_x^{1/2}$ is displacement noise with units of $\t{m}/\sqrt{\t{Hz}}$, $S_\nu^{1/2}$ is frequency noise with units of $\t{Hz}/\sqrt{\t{Hz}}$, etc.

\section{Permissions and Attributions}

\begin{enumerate}

\item The content of Sec.~\ref{sec:classical-noise} is the result of discussions with Rana Adhikari, Yanbei Chen, Gautam Venugopalan, Yuntao Bai, Aaron Markowitz, Chris Wipf, and Haixing Miao.

\item The content of Chap.~\ref{chap:hypotest} is the result of a collaboration with Zachary Mark, Rana Adhikari, and Yanbei Chen and is based off of a rough draft of a paper to be submitted soon.

\item The content of Chap.~\ref{chap:TianGO} is the result of a collaboration with Rana Adhikari and Geoffrey Lovelace and is based off of a rough draft of a paper to be submitted soon.

\item The content of Appendix.~\ref{chap:homodyne} is a slightly more detailed analysis of an idea due to Koji Arai~\cite{PolarizationBHD} which is necessary for part of a noise analysis in Sec.~\ref{sec:ponder-tech}.

\end{enumerate}

% \begin{figure}
%   \centering
%   \begin{minipage}{0.47\textwidth}
%     \includegraphics[width=\textwidth]{Chapters/Introduction/Figures/aLIGO_NB.pdf}
%   \end{minipage}\hfill
%   \begin{minipage}{0.47\textwidth}
%     \includegraphics[width=\textwidth]{Chapters/Introduction/Figures/Voyager_NB.pdf}
%   \end{minipage}
% \caption{blap}
% \end{figure}

% \begin{figure}
% \centering
% \caption{Network of ground and space gravitational wave detectors}
% \label{fig:detector-network}
% \end{figure}


% Cosmic explorer~\cite{Abbott2017d, Dwyer2015}. Einstein telescope
