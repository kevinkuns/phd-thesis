from __future__ import division
import numpy as np
import gwinc
import matplotlib as mpl
import subprocess

mpl.rcParams.update({'text.usetex': True,
                     'font.family': 'serif',
                     # 'font.serif': 'Georgia',
                     # 'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 22,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'legend.fancybox': True,
                     'legend.fontsize': 18,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

savefigs = True


ff = np.logspace(np.log10(5), np.log10(5000), 1000)


def computeNoises(ff, ifoName):
    ifo = gwinc.load_ifo(ifoName)
    ifo = gwinc.precompIFO(ff, ifo)
    noises = gwinc.noise_calc(ff, ifo)
    return ifo, noises


def plotNoises(ifo, noises, ifoName):
    fig = gwinc.plot_noise(ifo, noises, displacement=False)
    fig.gca().set_ylabel(r'Strain $[1/\mathrm{Hz}^{1/2}]$')
    fig.gca().set_title('{:s} Noise Budget'.format(ifoName))
    fig.set_size_inches(12, 9)
    return fig


ifo_aLIGO, noises_aLIGO = computeNoises(ff, 'aLIGO')
ifo_Voyager, noises_Voyager = computeNoises(ff, 'Voyager')


fig_aLIGO = plotNoises(ifo_aLIGO, noises_aLIGO, 'aLIGO Design Sensitivivity')
fig_Voyager = plotNoises(ifo_Voyager, noises_Voyager, 'Voyager')


if savefigs:
    for fig, fname in zip([fig_aLIGO, fig_Voyager], ['aLIGO', 'Voyager']):
        sname = 'Figures/{:s}_NB.pdf'.format(fname)
        fig.savefig(sname)
        subprocess.call(['pdfcrop', sname, sname])
