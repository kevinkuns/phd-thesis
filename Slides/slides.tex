\documentclass[serif, aspectratio=169]{beamer}
\usepackage{../commands}
%\usepackage[]{media9}
%\usepackage{multimedia}
\usepackage{hyperref}
\usepackage[absolute, overlay]{textpos}

\hyperblue

% \setbeamertemplate{footline}{}
% \newcommand{\footnoteblock}[1]{%
%   \begin{textblock*}{11cm}(1cm,9.1cm)%
%     \footnotesize#1%
%   \end{textblock*}%
% }


\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}


\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

% \setbeamertemplate{section in toc}[sections numbered]

\title{Future Networks of Gravitational Wave Detectors:\\
Quantum Noise and Space Detectors}
\author{Kevin Kuns}
\date{March 8, 2019}

\begin{document}


\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{Gravitational Waves Detected to Date}
\includegraphics[width=\textwidth]{Figures/SourceCatalog.png}
\end{frame}


\begin{frame}
\frametitle{Network of Ground and Space Detectors}
\begin{figure}
\centering
%\includegraphics[height=0.9\textheight]{../Chapters/Introduction/Figures/Network.pdf}
\only<1>{\includegraphics[width=0.9\textwidth]{Figures/Network0.png}}
\only<2>{\includegraphics[width=0.9\textwidth]{Figures/Network1.png}}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Quantum Noise}
\vspace{-0.4cm}
\begin{figure}
\centering
\includegraphics[height=0.85\textheight]{Figures/QuantumNoiseEllipses.png}
\end{figure}
\vspace{-1cm}
\blfootnote{Danilishin and Khalili, LRR, 15, (2012), 5}
\end{frame}

\begin{frame}
\frametitle{Ponderomotive squeezing}
\begin{columns}
  \column{0.5\textwidth}
    \vspace{-1cm}
    \begin{figure}
      \centering
      \includegraphics[scale=0.25]{Figures/PonderSqz.pdf}
    \end{figure}
  \column{0.5\textwidth}
    \begin{itemize}
      \item Radiation pressure force $F_\t{BA} = 2P/c$
      \item Acts on the mirror like
        \[-M\Omega^2 x = F_\t{BA}\]
      \item Amplitude fluctuations converted to phase fluctuations
        \begin{align*}
          b_1 &= a_1\\
          b_2 & = a_2 - \mathcal{K} a_1
        \end{align*}
      \item Where optomechanical coupling is
        \[\mathcal{K} \propto \frac{P}{M\Omega^2}\]
    \end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Phase sensitive optomechanical amplifier}
\begin{columns}
  \column{0.45\textwidth}
    \begin{figure}
      \centering
      \includegraphics[height=0.85\textheight]{../Chapters/QuantumNoise/Figures/AmplifierOverviewFig.pdf}
    \end{figure}
  \column{0.55\textwidth}
    \begin{itemize}
      \item In order to make use of highly squeezed states, need to protect them from classical noise and losses.
      \item Can use a phase sensitive optomechanical amplifier to amplify the signals before the large sources of loss
      \item Phase sensitive is important so that quantum noise is not added in the process.
      \item If the signal comes out of the IFO in $b_{\t{IFO},1}$ it is amplified to $b_2$:
    \[\bbmat
    b_1\\
    b_2
    \ebmat = 
    \bbmat
    1 & 0\\
    -\mathcal{K} & 1\ebmat
    \bbmat
    b_{\t{IFO},1}\\
    b_{\t{IFO},2}
    \ebmat\]
  \end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Ring cavity amplifier}
\begin{figure}
\includegraphics[scale=0.35]{../Chapters/QuantumNoise/Figures/RingCavity.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Can we observe ponderomotive squeezing in a large scale experiment?}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{Figures/IFOs.png}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Looking for Squeezing in a 40~m Interferometer}
\begin{figure}
\centering
\includegraphics[scale=0.7]{Figures/DRFPMI.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{40~m Noise Budget}
\begin{figure}
\centering
\includegraphics[height=0.9\textheight]{../Chapters/40m/Figures/DisplacementNoise.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Summary}
\begin{itemize}
\item Quantum noise is the dominant source of noise in all GW detectors at high frequencies.
\item Efforts to reduce this noise are limited by losses and classical noise.
\item The radiation pressure mediated pondermotive squeezing that naturally occurs in optomechanical systems can be used to ameliorate these issues
\item It is plausible the pondermotive squeezing can be observed in the near future with a large scale 40~m interferometer.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Detector Network Again}
\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{Figures/Network0.png}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Cosmological Reach}
\begin{figure}
\centering
\includegraphics[height=0.9\textheight]{../Chapters/Introduction/Figures/horizons.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Sky Localization}
\begin{columns}
  \column{0.5\textwidth}
  \begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{Figures/CANDELS.jpg}
  \end{figure}
  \begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{Figures/GroundErrorEllipses.png}
  \end{figure}
  \column{0.5\textwidth}
  \begin{itemize}
    \item Excellent sky localization ability:
      \begin{enumerate}
        \item Time dependent antenna patterns and Doppler shifts
        \item Large baseline for triangulation with ground detectors
      \end{enumerate}
    \item First BNS was localized to $28\,\t{deg}^2$; TianGO could have done of order $10^{-5}\,\t{deg}^2$.
    \item TianGO will see sources before they appear in the ground detector's bands and can serve as an early warning for both ground GW detectors and electromagnetic telescopes.
  \end{itemize}
\end{columns}
\end{frame}
  

\begin{frame}
\frametitle{Measuring the Hubble Constant}
\begin{columns}
  \column{0.5\textwidth}
  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{Figures/CANDELS_zoom.png}
  \end{figure}
  \column{0.5\textwidth}
  \begin{itemize}
    \item There is a $3\sigma$ tension between the two ways of currently measuring the Hubble constant.
    \item The detection of gravitational waves estimates the luminosity distance directly, but not the redshift.
    \item If the host galaxy can be identified, the redshift can be measured optically.
    \item Rely on electromagnetic counterparts with poor localization
  \end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Parameter Estimation}
\begin{columns}
  \column{0.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=1.1\textwidth]{Figures/PE_example.pdf}
    \end{figure}
  \column{0.5\textwidth}
    \begin{itemize}
      \item Want to estimate parameters $\theta = (M_1, M_1, D_L, \alpha, \delta, \dots)$
      \item $\theta_0$ are true parameters. The signal is
        \[s = h(\theta_0) + n,\]
        where $h$ is the model waveform and $n$ is detector noise
      \item The posterior probability is
        \[P(\theta|s) \propto \e^{-(s - h(\theta)|s - h(\theta))/2}\]
      \item Dot product is
        \[(A|B) = 4\re \int_0^\infty \frac{A^*(f)B(f)}{S(f)}\d f\]
    \end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Difficulties with Parameter Estimation}
\begin{columns}
  \column{0.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[width=1.1\textwidth]{../Chapters/Waveforms/Figures/WaveformAmplitude.pdf}
    \end{figure}
  \column{0.5\textwidth}
    Can compute $\e^{-(s-h|s-h)/2}$ in two ways:
    \begin{enumerate}
      \item Analytic quadratic approximation (Fisher matrix)
        \begin{itemize}
          \item Frequently encounter ill-conditioned matrices
        \end{itemize}
      \item Directly sample using Monte Carlo techniques
        \begin{itemize}
          \item Multiple sharp peaks are difficult to sample
        \end{itemize}
    \end{enumerate}
    Both exacerbated by complicated waveforms 
    \begin{itemize}
      \item High frequencies need merger and ringdown $\Rightarrow$ no PN expansion
      \item Low frequncies need time-dependent antenna patterns and Doppler shifts
    \end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{TianGO Design}
\begin{figure}
\centering
\includegraphics[height=0.87\textheight]{../Chapters/TianGO/Figures/TianGO_overview.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{LISA Orbit}
\begin{center}
\href{run:Figures/LISA_orbit.mp4}{
\includegraphics[scale=0.8]{Figures/LISA_orbit.pdf}}
\end{center}
\blfootnote{LISA Yellow Book}
\end{frame}

\begin{frame}
\frametitle{Arm length change in uncontrolled orbit}
\begin{columns}
  \column{0.5\textwidth}
    \begin{figure}
      \centering
      \includegraphics[height=0.8\textheight]{../Chapters/TianGO/Figures/OrbitCombined.pdf}
    \end{figure}
  \column{0.5\textwidth}
    \begin{itemize}
      \item Arm lengths change due to eccentricity of orbits and perturbations from the Earth and other planets.
      \item Frequency noise is challenging without the common mode rejection of a traditional interferometer
      \item Doppler shifts also hard to accommodate.
      \item TianGO uses simple Michelson and controls arm lengths to avoid these challenges
    \end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Summary}
\begin{itemize}
  \item TianGO is a relatively cheap and simple proposal for a space detector in the band from 10~mHz to 100~Hz.
  \item It's sky localization ability can serve as early warning for electromagnetic and ground GW detectors. Especially useful for investigating the tension between measurements of the Hubble constant
  \item Need a detailed study of TianGO's parameter estimation ability.
  \item Need to study orbits in more detail.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{TianGO Noise Budget}
\begin{figure}
\centering
\includegraphics[height=0.87\textheight]{../Chapters/TianGO/Figures/TianGOnb.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Optical Spring}
\begin{figure}
\centering
\includegraphics[height=0.87\textheight]{Figures/OpticalSpring.pdf}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{DARM Transfer Functions}
\begin{figure}
\centering
\includegraphics[height=0.9\textheight]{../Chapters/40m/Figures/darmTFs.pdf}
\end{figure}
\end{frame}

\end{document}
