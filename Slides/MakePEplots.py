from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from cbgw.waveforms import Waveform, theta
from cbgw.detector import Detector
import cbgw.binary_params as bp
from copy import deepcopy
import subprocess

plt.ion()

mpl.rcParams.update({'text.usetex': False,
                     'font.family': 'serif',
                     'font.serif': ['Georgia'],
                     'mathtext.fontset': 'cm',
                     'lines.linewidth': 2.5,
                     'font.size': 14,
                     'xtick.labelsize': 'large',
                     'ytick.labelsize': 'large',
                     'axes.labelsize': 'large',
                     'axes.titlesize': 'large',
                     'lines.markersize': 12,
                     'legend.borderpad': 0.2,
                     'legend.fancybox': True,
                     'legend.fontsize': 16,
                     'legend.framealpha': 0.7,
                     'legend.handletextpad': 0.5,
                     'legend.labelspacing': 0.2,
                     'legend.loc': 'best',
                     'savefig.dpi': 80,
                     'pdf.compression': 9})

savefigs = True


theta0 = deepcopy(theta)
theta1 = deepcopy(theta)

Mc1 = bp.chirp_mass(250, 250)
theta1[0] = Mc1
Mc0 = bp.chirp_mass(150, 150)
qq0 = 2
theta0[0] = Mc0
theta0[1] = qq0

ff = np.logspace(-2, 2, 1000)
det = Detector('Z1')

wf0 = Waveform(theta0, ff, 10)
ff0, hh0 = det.projectWaveform(wf0)
_, nn = det.generateNoise(ff0, seed=0)
nn_complex = nn[0, :]*np.exp(1j*nn[1, :])
hh_complex = hh0[0, :]*np.exp(1j*hh0[1, :])
ss_mag = np.abs(hh_complex + nn_complex)

wf1 = Waveform(theta1, ff, 10)
ff1, hh1 = det.projectWaveform(wf1)

fig = plt.figure()
ax = fig.gca()
# ax.loglog(ff0, hh0[0, :])
ax.loglog(ff0, ss_mag, label=r'data $s$')
ax.loglog(ff1, hh1[0, :], label=r'model $h(\theta)$')
ax.legend()
ax.grid(True, which='major', alpha=0.5)
ax.yaxis.set_minor_locator(plt.NullLocator())
ax.xaxis.grid(True, which='minor', alpha=0.2)
ax.set_xlabel('Frequency [Hz]')
ax.set_ylabel('Strain [1/Hz]')
ax.set_xlim(ff0[0], ff0[-1])
fig.tight_layout()
if savefigs:
    sname = 'Figures/PE_example.pdf'
    fig.savefig(sname)
    subprocess.call(['pdfcrop', sname, sname])
